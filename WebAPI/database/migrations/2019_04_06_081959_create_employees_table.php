<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('EmployeeID');
            $table->string('FullName');
            $table->string('EMPCode');
            $table->string('Mobile');
            $table->decimal('Balance', 8, 2);
            $table->enum('Status',['0', '1', '2'])->default('0');//comment('0'=>'No Balance', '1'=>"Advance", '2'=>"Previous Balance")
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
