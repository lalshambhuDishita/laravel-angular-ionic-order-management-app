<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->bigIncrements('OrderItemID');
            $table->integer('ItemID')->unsigned();
            $table->bigInteger('OrderID')->unsigned();
            $table->integer('Quantity');
            $table->timestamps();
        });

        Schema::table('order_items', function($table) {
            $table->foreign('ItemID')->references('ItemID')->on('items');
            $table->foreign('OrderID')->references('OrderID')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
