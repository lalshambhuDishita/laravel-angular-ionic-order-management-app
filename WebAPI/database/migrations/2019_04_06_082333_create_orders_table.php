<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('OrderID');
            $table->string('OrderNo');
            $table->integer('EmployeeID')->unsigned();
            $table->string('PMethod');
            $table->decimal('GTotal', 8, 2);
            $table->enum('Status',['0', '1'])->default('1');//comment('0'=>'Deleted', '1'=>"Active")
            $table->timestamps();
        });

        Schema::table('orders', function($table) {
            $table->foreign('EmployeeID')->references('EmployeeID')->on('employees');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
