<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'OrderID';
    protected $fillable = ['OrderID', 'OrderNo', 'EmployeeID', 'PMethod', 'GTotal'];
    protected $maps = ['DeletedOrderItemIDs'=>'dummy_DeletedOrderItemIDs'];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItems', 'OrderID');
    }
    public function employee()
    {
        return $this->belongsTo('App\Employee', 'EmployeeID');
    }
    public function orderItemsDelete(){
        return $this->hasMany('App\OrderItems', 'OrderID');
    }
    public static function boot() {
        parent::boot();

        static::deleting(function($order) { // before delete() method call this
             $order->orderItemsDelete()->delete();
             // do the rest of the cleanup...
        });
    }
}
