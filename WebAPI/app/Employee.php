<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Employee extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard = 'employee';


    protected $primaryKey = 'EmployeeID';
    protected $fillable = ['EmployeeID','email', 'password', 'FullName', 'EMPCode', 'Mobile', 'Balance', 'Status'];

    public function orders()
    {
        return $this->hasMany('App\Order', 'OrderID' );
    }

/**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
}
