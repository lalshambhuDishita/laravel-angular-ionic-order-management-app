<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $primaryKey = 'ItemID';
    protected $fillable = ['ItemID', 'Name', 'Price', 'Image'];

    public function orderItems()
    {
        return $this->hasMany('App\OrderItems', 'OrderItemID', 'ItemID');
    }
}
