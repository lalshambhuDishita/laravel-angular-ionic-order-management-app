<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $primaryKey = 'OrderItemID';
    protected $fillable = ['OrderItemID', 'OrderID', 'ItemID', 'Quantity'];

    public function order()
    {
        return $this->belongsTo('App\Order', 'OrderID');
    }
    public function item()
    {
        return $this->belongsTo('App\Item', 'ItemID');
    }
}
