<?php

namespace App\Http\Controllers;
use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request){
        
        $http = new \GuzzleHttp\Client();
        
        try {
            $response = $http->post('http://empployee.local/oauth/token', [
                'form_params'=> [
                    'grant_type' =>'password',
                    'client_id' =>2,
                    'client_secret' =>'s1909MFWplyQ16bDz0akRcQDvHjsqIYRkJWxx7YF',
                    'username' =>$request->username,
                    'password' =>$request->password,
                    
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if($e->getCode()==400){
                return $response()->json('Invalid Request. Please Enter Username or Password'. $e->getCode());
            }else if($e->getCode()==401){
                return $response->json('Your Credentials are incorrect. Please try again'. $e->getCode());
            }

            return $response()->json('Something went wrong on the server'. $e->getCode());
        }
    }

    public function register(Request $request){
        $request->validate( [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8']
        ]);

        return User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
    }

    public function registerCustomer(Request $request){
        $request->validate( [
            'FullName' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:customers'],
            'password' => ['required', 'string', 'min:8']
        ]);

        return Customer::create([
            'FullName' => $request->FullName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'EMPCode' => $request->EMPCode,
            'Mobile' => $request->Mobile,
            'Balance' => $request->Balance
        ]);
    }


    public function logout(){
        auth()->user()->tokens->each(function($token, $key){
            $token->delete();
        });
        return response()->json('Logged Out successfully', 200);
    }
}
