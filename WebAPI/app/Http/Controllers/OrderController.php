<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Item;
use App\OrderItems;

class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::where('Status', "1")->with('employee')->get();;
        return $order;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        

            $order = Order::with('orderItems')->firstOrNew(['OrderID' => $request->OrderID]);
            
            //return $order->DeletedOrderItemIDs;
            $x = $request->DeletedOrderItemIDs;
            $arr = explode(",", $x);
            foreach ($arr as $key => $value) {
                if($value!=''){
                    $orderitems = OrderItems::findOrFail($value);
                    $orderitems->delete();
                }
            }
            // $array_diff[] = array_diff($order->orderItems[0], $request->OrderItems[0]);
            // return $array_diff;
            $order->OrderID = $request->OrderID;
            $order->OrderNo = $request->OrderNo;
            $order->EmployeeID = $request->EmployeeID;
            $order->PMethod = $request->PMethod;
            $order->GTotal = $request->GTotal;
            //return  $request->OrderItems[0];
            if($order->save()){
                
                foreach($request->OrderItems as $orderITEM){
                    $orderIteme = OrderItems::firstOrNew(array('OrderItemID' => $orderITEM["OrderItemID"]));
                    //$orderIteme->OrderItemID = $orderITEM["OrderItemID"];
                    $orderIteme->OrderID = $order->OrderID;
                    $orderIteme->ItemID = $orderITEM["ItemID"];
                    $orderIteme->Quantity = $orderITEM["Quantity"];
                    $orderIteme->save();
                }
                
            }

            

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $order = Order::with('employee')->find($id);
        $orderDetails = OrderItems::with(
            array(
                    
                    'item'=>function($query2){
                        $query2->select('ItemID','Name as ItemName', 'Price');
                    }
                )
            )->where('OrderID', $id)->get();

            $order['FullName'] = $order['employee']['FullName'];
            $order['EMPCode'] = $order['employee']['EMPCode'];
            $order['Mobile'] = $order['employee']['Mobile'];
            $order['Position'] = $order['employee']['Position'];
            $order['Balance'] = $order['employee']['Balance'];
            $order['Status'] = $order['employee']['Status'];
            unset($order['employee']);

            foreach ($orderDetails as $key => $value) {
                
                $orderDetails[$key]['Quantity'] = $value['Quantity'];
                $orderDetails[$key]['ItemName'] = $value['item']['ItemName'];
                $orderDetails[$key]['Price'] = $value['item']['Price'];
                $orderDetails[$key]['Total'] = $value['item']['Price']*$value['Quantity'];
                unset($value['item']);
            }


        return array('order'=>$order,'item'=>$orderDetails);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $input = $request->all();
    //     $orderID = $request->input('OrderID');
    //     $order =Order::where("orderID",$orderID)->update($input);
    //     return response($order);
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyorder(Request $request, $orderid)
    {
        $order = Order::findOrFail($orderid);
        $order->Status = '0';
        $order->save();
        // $order = Order::findOrFail($orderid);
        // $order->orderItemsDelete()->delete();
        // $order->delete();

        return 204;
    }
}
