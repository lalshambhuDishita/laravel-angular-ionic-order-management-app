<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getItems()
    {
        $items = Item::all();
        return $items;
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       if ($request->hasFile('Image'))
        {
            $file      = $request->file('Image');
            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $picture   = date('His').'-'.$filename;
            $file->move(public_path('Images/Items'), $picture);

            $items = new Item;
            $items->Name = $request->get('Name');
            $items->Price = $request->get('Price');
            $items->Image = $picture;
            $items->save();
            return response()->json(["message" => "Image Uploaded Succesfully", "items"=> $items]);
        } else{
            return response()->json(["message" => $request->all()]);
        }
        
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $itemID)
    {
        //echo $empID;
        $item = Item::findOrFail($itemID);
        $item->delete();

        return 204;
    }

    public function updateItem(Request $request)
    {
        $input = $request->all();
        $itemID = $request->input('ItemID');
        $item =Item::where("ItemID",$itemID)->update($input);
        return response($item);
    }
}
