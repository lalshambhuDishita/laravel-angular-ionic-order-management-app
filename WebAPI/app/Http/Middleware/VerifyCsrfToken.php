<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://empployee.local/api/storeEmployees',
        'http://empployee.local/api/employeesList',
        'http://empployee.local/api/updateEmployee',
        'http://empployee.local/api/deleteEmployee/*',
        'http://empployee.local/api/itemsList',
        'http://empployee.local/api/orders',
        'http://empployee.local/api/destroyorder/*',
        'http://empployee.local/api/storeItem',
        'http://empployee.local/api/updateItem',
        'http://empployee.local/api/deleteItem/*',
        'http://empployee.local/api/showEmployee/*',
        
    ];
    
}
