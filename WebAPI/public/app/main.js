(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule, routingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingComponents", function() { return routingComponents; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _employees_employees_employees_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./employees/employees/employees.component */ "./src/app/employees/employees/employees.component.ts");
/* harmony import */ var _employees_employees_employee_list_employee_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./employees/employees/employee-list/employee-list.component */ "./src/app/employees/employees/employee-list/employee-list.component.ts");
/* harmony import */ var _employees_employees_employee_employee_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./employees/employees/employee/employee.component */ "./src/app/employees/employees/employee/employee.component.ts");
/* harmony import */ var _orders_orders_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./orders/orders.component */ "./src/app/orders/orders.component.ts");
/* harmony import */ var _orders_order_order_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./orders/order/order.component */ "./src/app/orders/order/order.component.ts");
/* harmony import */ var _orders_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./orders/print-layout/print-layout.component */ "./src/app/orders/print-layout/print-layout.component.ts");
/* harmony import */ var _items_items_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./items/items.component */ "./src/app/items/items.component.ts");
/* harmony import */ var _items_item_list_item_list_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./items/item-list/item-list.component */ "./src/app/items/item-list/item-list.component.ts");
/* harmony import */ var _items_item_item_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./items/item/item.component */ "./src/app/items/item/item.component.ts");
/* harmony import */ var _history_history_employee_order_history_employee_order_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./history/history-employee-order/history-employee-order.component */ "./src/app/history/history-employee-order/history-employee-order.component.ts");
/* harmony import */ var _history_history_order_details_history_order_details_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./history/history-order-details/history-order-details.component */ "./src/app/history/history-order-details/history-order-details.component.ts");
/* harmony import */ var _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./user/sign-in/sign-in.component */ "./src/app/user/sign-in/sign-in.component.ts");
/* harmony import */ var _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./user/sign-up/sign-up.component */ "./src/app/user/sign-up/sign-up.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");



















var routes = [
    { path: "", redirectTo: "login", pathMatch: 'full' },
    {
        path: "signup", component: _user_user_component__WEBPACK_IMPORTED_MODULE_16__["UserComponent"],
        children: [{ path: "", component: _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_15__["SignUpComponent"] }]
    },
    {
        path: "login", component: _user_user_component__WEBPACK_IMPORTED_MODULE_16__["UserComponent"],
        children: [{ path: "", component: _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_14__["SignInComponent"] }]
    },
    { path: "user", component: _user_user_component__WEBPACK_IMPORTED_MODULE_16__["UserComponent"] },
    { path: "navbar", component: _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_17__["NavbarComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: "employees", component: _employees_employees_employees_component__WEBPACK_IMPORTED_MODULE_3__["EmployeesComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: "items", component: _items_items_component__WEBPACK_IMPORTED_MODULE_9__["ItemsComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: "orders", component: _orders_orders_component__WEBPACK_IMPORTED_MODULE_6__["OrdersComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: "order", children: [
            { path: "", component: _orders_order_order_component__WEBPACK_IMPORTED_MODULE_7__["OrderComponent"] },
            { path: "edit/:id", component: _orders_order_order_component__WEBPACK_IMPORTED_MODULE_7__["OrderComponent"] }
        ], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: "history", children: [
            { path: "", component: _history_history_employee_order_history_employee_order_component__WEBPACK_IMPORTED_MODULE_12__["HistoryEmployeeOrderComponent"] },
            { path: "employee/:id", component: _history_history_order_details_history_order_details_component__WEBPACK_IMPORTED_MODULE_13__["HistoryOrderDetailsComponent"] }
        ], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] },
    { path: 'print',
        outlet: 'print',
        component: _orders_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_8__["PrintLayoutComponent"],
        children: [
            { path: 'order/edit/:id', component: _orders_order_order_component__WEBPACK_IMPORTED_MODULE_7__["OrderComponent"], canActivate: [_auth_auth_guard__WEBPACK_IMPORTED_MODULE_18__["AuthGuard"]] }
        ]
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

var routingComponents = [_employees_employees_employees_component__WEBPACK_IMPORTED_MODULE_3__["EmployeesComponent"], _employees_employees_employee_list_employee_list_component__WEBPACK_IMPORTED_MODULE_4__["EmployeeListComponent"], _employees_employees_employee_employee_component__WEBPACK_IMPORTED_MODULE_5__["EmployeeComponent"], _orders_orders_component__WEBPACK_IMPORTED_MODULE_6__["OrdersComponent"], _items_items_component__WEBPACK_IMPORTED_MODULE_9__["ItemsComponent"], _items_item_list_item_list_component__WEBPACK_IMPORTED_MODULE_10__["ItemListComponent"], _items_item_item_component__WEBPACK_IMPORTED_MODULE_11__["ItemComponent"], _history_history_employee_order_history_employee_order_component__WEBPACK_IMPORTED_MODULE_12__["HistoryEmployeeOrderComponent"], _user_user_component__WEBPACK_IMPORTED_MODULE_16__["UserComponent"], _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_14__["SignInComponent"], _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_15__["SignUpComponent"], _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_17__["NavbarComponent"]];


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<app-navbar></app-navbar>\n<div class=\"jumbotron jumbtron-fluid\">\n  \n  <div class=\"container containe-fluid\">\n    <div class=\"row\">\n      <div class=\"col-md-12\">\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n  </div>      \n</div>\n<router-outlet name=\"print\"></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var angular_font_awesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular-font-awesome */ "./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/esm5/radio.es5.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/esm5/tabs.es5.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _employees_employees_employees_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./employees/employees/employees.component */ "./src/app/employees/employees/employees.component.ts");
/* harmony import */ var _employees_employees_employee_employee_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./employees/employees/employee/employee.component */ "./src/app/employees/employees/employee/employee.component.ts");
/* harmony import */ var _employees_employees_employee_list_employee_list_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./employees/employees/employee-list/employee-list.component */ "./src/app/employees/employees/employee-list/employee-list.component.ts");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _orders_orders_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./orders/orders.component */ "./src/app/orders/orders.component.ts");
/* harmony import */ var _orders_order_order_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./orders/order/order.component */ "./src/app/orders/order/order.component.ts");
/* harmony import */ var _orders_order_items_order_items_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./orders/order-items/order-items.component */ "./src/app/orders/order-items/order-items.component.ts");
/* harmony import */ var _shared_order_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/order.service */ "./src/app/shared/order.service.ts");
/* harmony import */ var _shared_user_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./shared/user.service */ "./src/app/shared/user.service.ts");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/navbar/navbar.component.ts");
/* harmony import */ var _items_items_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./items/items.component */ "./src/app/items/items.component.ts");
/* harmony import */ var _items_item_item_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./items/item/item.component */ "./src/app/items/item/item.component.ts");
/* harmony import */ var _items_item_list_item_list_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./items/item-list/item-list.component */ "./src/app/items/item-list/item-list.component.ts");
/* harmony import */ var _orders_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./orders/print-layout/print-layout.component */ "./src/app/orders/print-layout/print-layout.component.ts");
/* harmony import */ var _history_history_employee_order_history_employee_order_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./history/history-employee-order/history-employee-order.component */ "./src/app/history/history-employee-order/history-employee-order.component.ts");
/* harmony import */ var _history_history_order_details_history_order_details_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./history/history-order-details/history-order-details.component */ "./src/app/history/history-order-details/history-order-details.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./user/sign-in/sign-in.component */ "./src/app/user/sign-in/sign-in.component.ts");
/* harmony import */ var _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./user/sign-up/sign-up.component */ "./src/app/user/sign-up/sign-up.component.ts");
/* harmony import */ var _auth_auth_guard__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./auth/auth.guard */ "./src/app/auth/auth.guard.ts");

































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"],
                _employees_employees_employees_component__WEBPACK_IMPORTED_MODULE_12__["EmployeesComponent"],
                _employees_employees_employee_employee_component__WEBPACK_IMPORTED_MODULE_13__["EmployeeComponent"],
                _employees_employees_employee_list_employee_list_component__WEBPACK_IMPORTED_MODULE_14__["EmployeeListComponent"],
                _orders_orders_component__WEBPACK_IMPORTED_MODULE_17__["OrdersComponent"],
                _orders_order_order_component__WEBPACK_IMPORTED_MODULE_18__["OrderComponent"],
                _orders_order_items_order_items_component__WEBPACK_IMPORTED_MODULE_19__["OrderItemsComponent"],
                _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_22__["NavbarComponent"],
                _items_items_component__WEBPACK_IMPORTED_MODULE_23__["ItemsComponent"],
                _items_item_item_component__WEBPACK_IMPORTED_MODULE_24__["ItemComponent"],
                _items_item_list_item_list_component__WEBPACK_IMPORTED_MODULE_25__["ItemListComponent"],
                _orders_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_26__["PrintLayoutComponent"],
                _history_history_employee_order_history_employee_order_component__WEBPACK_IMPORTED_MODULE_27__["HistoryEmployeeOrderComponent"],
                _history_history_order_details_history_order_details_component__WEBPACK_IMPORTED_MODULE_28__["HistoryOrderDetailsComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_29__["UserComponent"],
                _user_sign_in_sign_in_component__WEBPACK_IMPORTED_MODULE_30__["SignInComponent"],
                _user_sign_up_sign_up_component__WEBPACK_IMPORTED_MODULE_31__["SignUpComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_7__["ToastrModule"].forRoot(),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_16__["AppRoutingModule"],
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_6__["MatDialogModule"],
                angular_font_awesome__WEBPACK_IMPORTED_MODULE_8__["AngularFontAwesomeModule"],
                _angular_material_radio__WEBPACK_IMPORTED_MODULE_9__["MatRadioModule"],
                _angular_material_tabs__WEBPACK_IMPORTED_MODULE_10__["MatTabsModule"]
            ],
            entryComponents: [_orders_order_items_order_items_component__WEBPACK_IMPORTED_MODULE_19__["OrderItemsComponent"]],
            providers: [src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_15__["EmployeeServiceService"], _shared_order_service__WEBPACK_IMPORTED_MODULE_20__["OrderService"], _shared_user_service__WEBPACK_IMPORTED_MODULE_21__["UserService"], _auth_auth_guard__WEBPACK_IMPORTED_MODULE_32__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_11__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.guard.ts":
/*!************************************!*\
  !*** ./src/app/auth/auth.guard.ts ***!
  \************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (next, state) {
        if (localStorage.getItem('userToken') != null)
            return true;
        this.router.navigate(['/login']);
        return false;
    };
    AuthGuard.prototype.canActivateChild = function (next, state) {
        return true;
    };
    AuthGuard.prototype.canLoad = function (route, segments) {
        return true;
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/employees/employees/employee-list/employee-list.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/employees/employees/employee-list/employee-list.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZXMvZW1wbG95ZWUtbGlzdC9lbXBsb3llZS1saXN0LmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/employees/employees/employee-list/employee-list.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/employees/employees/employee-list/employee-list.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-hover mat-table table-responsive\">\n  <tr>\n    <th>Name</th>\n    <th>Contact</th>\n    <th>Balance</th>\n    <th>Status</th>\n    <th>Operation</th>\n  </tr>\n  <tr *ngFor=\"let emp of service.list\">\n    <td (click)=\"populateForm(emp)\">[{{emp.EMPCode}}]-{{emp.FullName}}</td>\n    <td>{{emp.Mobile}}</td>\n    <td>{{emp.Balance}}</td>\n    <td>{{emp.Status}}</td>\n    <td >\n        <a class=\"btn btn-sm btn-info text-white\" routerLink=\"/history\" routerLinkActive=\"active\"><i class=\"fa fa-history\" area-hidden=\"true\"></i></a>\n      <button title=\"Edit\" (click)=\"populateForm(emp)\" class=\"btn btn-sm btn-info text-white ml-1\"><i class=\"fa fa-pencil\"></i></button>\n      <button title=\"Delete\" (click)=\"onDelete(emp.EmployeeID)\" class=\"btn btn-sm btn-danger text-white ml-1\"><i class=\"fa fa-trash\"></i></button>\n    </td>\n  </tr>\n</table>"

/***/ }),

/***/ "./src/app/employees/employees/employee-list/employee-list.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/employees/employees/employee-list/employee-list.component.ts ***!
  \******************************************************************************/
/*! exports provided: EmployeeListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListComponent", function() { return EmployeeListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");




var EmployeeListComponent = /** @class */ (function () {
    function EmployeeListComponent(service, toastr) {
        this.service = service;
        this.toastr = toastr;
    }
    EmployeeListComponent.prototype.ngOnInit = function () {
        this.service.refreshList();
    };
    EmployeeListComponent.prototype.populateForm = function (emp) {
        this.service.formData = Object.assign({}, emp);
    };
    EmployeeListComponent.prototype.onDelete = function (id) {
        var _this = this;
        if (confirm("Are you sure to delete this record?")) {
            this.service.deleteEmpployee(id).subscribe(function (res) {
                _this.service.refreshList();
                _this.toastr.warning("Employee Deleted succesfully", "Emp Deleted");
            });
        }
    };
    EmployeeListComponent.prototype.orderhistory = function (empID) {
    };
    EmployeeListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-employee-list',
            template: __webpack_require__(/*! ./employee-list.component.html */ "./src/app/employees/employees/employee-list/employee-list.component.html"),
            styles: [__webpack_require__(/*! ./employee-list.component.css */ "./src/app/employees/employees/employee-list/employee-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeServiceService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());



/***/ }),

/***/ "./src/app/employees/employees/employee/employee.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/employees/employees/employee/employee.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* You can add global styles to this file, and also import other style files */\r\ninput.ng-dirty.ng-invalid{\r\n    border-color: #dc3545;\r\n}\r\ndiv.validation-error{\r\n    width:100%;\r\n    margin-top: 25rem;\r\n    font-size: 80%;\r\n    color: #dc3545;\r\n}\r\nnav a:visited, a:link{\r\n    color: #607088;\r\n    \r\n}\r\nnav a:hover{\r\n    color: #039be5;\r\n    background-color: #cfd8dc;\r\n}\r\nnav a:active{\r\n    color: #039bef;\r\n    \r\n}\r\n.jumbotron{\r\n    padding-top: 0px;\r\n    background: #ffffff;\r\n}\r\n.form-control, .input-group-text{\r\n    border: 1px solid #0080ff;\r\n}\r\ntable .table-hover tbody tr:hover{\r\n    cursor: pointer;\r\n}\r\n.mat-radio-button ~ .mat-radio-button {\r\n    margin-left: 16px;\r\n  }\r\n  \r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZW1wbG95ZWVzL2VtcGxveWVlcy9lbXBsb3llZS9lbXBsb3llZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDhFQUE4RTtBQUM5RTtJQUNJLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksVUFBVTtJQUNWLGlCQUFpQjtJQUNqQixjQUFjO0lBQ2QsY0FBYztBQUNsQjtBQUNBO0lBQ0ksY0FBYzs7QUFFbEI7QUFDQTtJQUNJLGNBQWM7SUFDZCx5QkFBeUI7QUFDN0I7QUFDQTtJQUNJLGNBQWM7O0FBRWxCO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSx5QkFBeUI7QUFDN0I7QUFFQTtJQUNJLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGlCQUFpQjtFQUNuQiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZXMvZW1wbG95ZWUvZW1wbG95ZWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIFlvdSBjYW4gYWRkIGdsb2JhbCBzdHlsZXMgdG8gdGhpcyBmaWxlLCBhbmQgYWxzbyBpbXBvcnQgb3RoZXIgc3R5bGUgZmlsZXMgKi9cclxuaW5wdXQubmctZGlydHkubmctaW52YWxpZHtcclxuICAgIGJvcmRlci1jb2xvcjogI2RjMzU0NTtcclxufVxyXG5kaXYudmFsaWRhdGlvbi1lcnJvcntcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyNXJlbTtcclxuICAgIGZvbnQtc2l6ZTogODAlO1xyXG4gICAgY29sb3I6ICNkYzM1NDU7XHJcbn1cclxubmF2IGE6dmlzaXRlZCwgYTpsaW5re1xyXG4gICAgY29sb3I6ICM2MDcwODg7XHJcbiAgICBcclxufVxyXG5uYXYgYTpob3ZlcntcclxuICAgIGNvbG9yOiAjMDM5YmU1O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2NmZDhkYztcclxufVxyXG5uYXYgYTphY3RpdmV7XHJcbiAgICBjb2xvcjogIzAzOWJlZjtcclxuICAgIFxyXG59XHJcblxyXG4uanVtYm90cm9ue1xyXG4gICAgcGFkZGluZy10b3A6IDBweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbn1cclxuXHJcbi5mb3JtLWNvbnRyb2wsIC5pbnB1dC1ncm91cC10ZXh0e1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzAwODBmZjtcclxufVxyXG5cclxudGFibGUgLnRhYmxlLWhvdmVyIHRib2R5IHRyOmhvdmVye1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5tYXQtcmFkaW8tYnV0dG9uIH4gLm1hdC1yYWRpby1idXR0b24ge1xyXG4gICAgbWFyZ2luLWxlZnQ6IDE2cHg7XHJcbiAgfVxyXG4gIFxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/employees/employees/employee/employee.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/employees/employees/employee/employee.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #form=\"ngForm\" (submit)=\"onSubmit(form)\" autocomplete=\"off\">\n  <input type=\"hidden\" name=\"EmployeeID\" #EmployeeID=\"ngModel\" [(ngModel)]=\"service.formData.EmployeeID\">\n<div class=\"form-group\" ng-class=\"{'has-error': service.formData.FullName.$touched && service.formData.FullName.$error.required , 'has-success': service.formData.FullName.$valid }\">\n  <label for=\"FullName\" class=\" control-label\">Full Name</label>\n  <div>\n      <input name=\"FullName\" #FullName=\"ngModel\" [(ngModel)]=\"service.formData.FullName\" class=\"form-control\" required>\n  </div>\n  <div>\n      <span class=\"help-block\" *ngIf=\"FullName.invalid && FullName.touched\" ng-show=\"service.formData.FullName.$touched && service.formData.FullName.$error.required\">Please enter Full Name.</span>\n  </div>\n</div>\n  <div class=\"form-group\">\n    <label>Pin Code</label>\n    <div>\n        <input name=\"EMPCode\" #EMPCode=\"ngModel\" [(ngModel)]=\"service.formData.EMPCode\" class=\"form-control\" required >\n    </div>\n    <div role=\"alert\">\n        <span class=\"help-block\" *ngIf=\"EMPCode.invalid && EMPCode.touched\"  ng-show=\"service.formData.EMPCode.$touched && service.formData.EMPCode.$error.required\">Please enter valid pincode.</span>\n    </div>\n    \n  </div>\n  <div class=\"form-row\">\n      <div class=\"form-group col-md-6\">\n        <label>Mobile</label>\n        <input name=\"Mobile\" #Mobile=\"ngModel\" [(ngModel)]=\"service.formData.Mobile\" class=\"form-control\">\n      </div>\n      <div class=\"form-group col-md-6\" ><!--*ngIf=\"!service.formData.EmployeeID\">-->\n        <label>Balance</label>\n        <div>\n            <input name=\"Balance\" #Balance=\"ngModel\" [(ngModel)]=\"service.formData.Balance\" class=\"form-control\" required >\n            <mat-radio-group name=\"Status\" #Status=\"ngModel\" [(ngModel)]=\"service.formData.Status\" aria-label=\"Select an option\">\n              <mat-radio-button value=\"0\"  (change)=\"RadioChange('0', service.formData.Balance)\">NB</mat-radio-button>\n              <mat-radio-button value=\"1\"  (change)=\"RadioChange('1',service.formData.Balance)\">A</mat-radio-button>\n              <mat-radio-button value=\"2\"  (change)=\"RadioChange('2', service.formData.Balance)\">PB</mat-radio-button>\n            </mat-radio-group>\n        </div>\n        <div role=\"alert\">\n            <span class=\"help-block\" *ngIf=\"Balance.invalid && Balance.touched\"  ng-show=\"service.formData.Balance.$touched && service.formData.Balance.$error.required\">Please enter Balance.</span>\n        </div>\n        \n      </div>\n  </div>\n  <div class=\"form-group\">\n    <button type=\"submit\" [disabled]=\"form.invalid\" class=\"btn btn-lg btn-block\">Submit</button>\n  </div>\n  \n</form>"

/***/ }),

/***/ "./src/app/employees/employees/employee/employee.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/employees/employees/employee/employee.component.ts ***!
  \********************************************************************/
/*! exports provided: EmployeeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeComponent", function() { return EmployeeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");




var EmployeeComponent = /** @class */ (function () {
    function EmployeeComponent(service, toastr) {
        this.service = service;
        this.toastr = toastr;
    }
    EmployeeComponent.prototype.ngOnInit = function () {
        this.resetForm();
    };
    EmployeeComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.resetForm();
        this.service.formData = {
            EmployeeID: null,
            FullName: '',
            EMPCode: '',
            Mobile: '',
            Balance: 0,
            Status: 0,
            email: '',
            password: ''
        };
    };
    EmployeeComponent.prototype.onSubmit = function (form) {
        if (form.value.EmployeeID == null)
            this.insertRecord(form);
        else
            this.updateRecord(form);
    };
    EmployeeComponent.prototype.insertRecord = function (form) {
        var _this = this;
        this.service.postEmployee(form.value).subscribe(function (res) {
            _this.toastr.success("Inserted Successfully", "Emp Register");
            _this.resetForm(form);
            _this.service.refreshList();
        });
    };
    EmployeeComponent.prototype.updateRecord = function (form) {
        var _this = this;
        this.service.putEmployee(form.value).subscribe(function (res) {
            //console.log(res);
            _this.toastr.info("Updated Successfully", "Emp Updated");
            _this.resetForm(form);
            _this.service.refreshList();
        });
    };
    EmployeeComponent.prototype.addmoreadvance = function () {
        var st = "ok";
        Object.assign({}, st);
        console.log(st);
    };
    EmployeeComponent.prototype.RadioChange = function (ctrl, Bal) {
        if (ctrl == 0 || ctrl == "0") {
            this.service.formData.Balance = 0;
        }
    };
    EmployeeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-employee',
            template: __webpack_require__(/*! ./employee.component.html */ "./src/app/employees/employees/employee/employee.component.html"),
            styles: [__webpack_require__(/*! ./employee.component.css */ "./src/app/employees/employees/employee/employee.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeServiceService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], EmployeeComponent);
    return EmployeeComponent;
}());



/***/ }),

/***/ "./src/app/employees/employees/employees.component.css":
/*!*************************************************************!*\
  !*** ./src/app/employees/employees/employees.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VtcGxveWVlcy9lbXBsb3llZXMvZW1wbG95ZWVzLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/employees/employees/employees.component.html":
/*!**************************************************************!*\
  !*** ./src/app/employees/employees/employees.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"jumbtron\">\n  <h4 class=\"display-6 text-center\">Customers</h4>\n  <hr>\n  \n  <div class=\"row\">\n    <div class=\"col-md-5\">\n      <app-employee></app-employee>\n    </div>\n    <div class=\"col-md-7\">\n      <app-employee-list></app-employee-list>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/employees/employees/employees.component.ts":
/*!************************************************************!*\
  !*** ./src/app/employees/employees/employees.component.ts ***!
  \************************************************************/
/*! exports provided: EmployeesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeesComponent", function() { return EmployeesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var EmployeesComponent = /** @class */ (function () {
    function EmployeesComponent(router) {
        this.router = router;
    }
    EmployeesComponent.prototype.ngOnInit = function () {
    };
    EmployeesComponent.prototype.logout = function () {
        localStorage.removeItem('userToken');
        this.router.navigate(["/login"]);
    };
    EmployeesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-employees',
            template: __webpack_require__(/*! ./employees.component.html */ "./src/app/employees/employees/employees.component.html"),
            styles: [__webpack_require__(/*! ./employees.component.css */ "./src/app/employees/employees/employees.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], EmployeesComponent);
    return EmployeesComponent;
}());



/***/ }),

/***/ "./src/app/history/history-employee-order/history-employee-order.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/history/history-employee-order/history-employee-order.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hpc3RvcnkvaGlzdG9yeS1lbXBsb3llZS1vcmRlci9oaXN0b3J5LWVtcGxveWVlLW9yZGVyLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/history/history-employee-order/history-employee-order.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/history/history-employee-order/history-employee-order.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-hover\">\n  <tr>\n    <th>Name</th>\n    <th>Contact</th>\n    <th>Balance</th>\n    <th>Status</th>\n    <th>Operation</th>\n  </tr>\n  <tr *ngFor=\"let emp of customerService.list\">\n    <td >[{{emp.EMPCode}}]-{{emp.FullName}}</td>\n    <td>{{emp.Mobile}}</td>\n    <td>{{emp.Balance}}</td>\n    <td>{{emp.Status}}</td>\n    <td >\n        <button (click)=\"openhistory(emp.EmployeeID)\" class=\"btn btn-sm btn-info text-white\"><i class=\"fa fa-eye\" area-hidden=\"true\"></i></button>\n      \n    </td>\n  </tr>\n</table>"

/***/ }),

/***/ "./src/app/history/history-employee-order/history-employee-order.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/history/history-employee-order/history-employee-order.component.ts ***!
  \************************************************************************************/
/*! exports provided: HistoryEmployeeOrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryEmployeeOrderComponent", function() { return HistoryEmployeeOrderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/order.service */ "./src/app/shared/order.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var HistoryEmployeeOrderComponent = /** @class */ (function () {
    function HistoryEmployeeOrderComponent(service, dialog, customerService, router, currentRoute) {
        this.service = service;
        this.dialog = dialog;
        this.customerService = customerService;
        this.router = router;
        this.currentRoute = currentRoute;
        this.isValid = true;
    }
    HistoryEmployeeOrderComponent.prototype.ngOnInit = function () {
        this.customerService.refreshList();
    };
    HistoryEmployeeOrderComponent.prototype.openhistory = function (EmployeeID) {
        this.router.navigate(['/history/employee/' + EmployeeID]);
    };
    HistoryEmployeeOrderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-history-employee-order',
            template: __webpack_require__(/*! ./history-employee-order.component.html */ "./src/app/history/history-employee-order/history-employee-order.component.html"),
            styles: [__webpack_require__(/*! ./history-employee-order.component.css */ "./src/app/history/history-employee-order/history-employee-order.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_4__["EmployeeServiceService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], HistoryEmployeeOrderComponent);
    return HistoryEmployeeOrderComponent;
}());



/***/ }),

/***/ "./src/app/history/history-order-details/history-order-details.component.css":
/*!***********************************************************************************!*\
  !*** ./src/app/history/history-order-details/history-order-details.component.css ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".card {\r\n    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);\r\n    max-width: 300px;\r\n    margin: auto;\r\n    text-align: center;\r\n    font-family: arial;\r\n  }\r\n  \r\n  .title {\r\n    color: grey;\r\n    font-size: 18px;\r\n  }\r\n  \r\n  button {\r\n    border: none;\r\n    outline: 0;\r\n    display: inline-block;\r\n    padding: 8px;\r\n    color: white;\r\n    background-color: #000;\r\n    text-align: center;\r\n    cursor: pointer;\r\n    width: 100%;\r\n    font-size: 18px;\r\n  }\r\n  \r\n  a {\r\n    text-decoration: none;\r\n    font-size: 22px;\r\n    color: black;\r\n  }\r\n  \r\n  button:hover, a:hover {\r\n    opacity: 0.7;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGlzdG9yeS9oaXN0b3J5LW9yZGVyLWRldGFpbHMvaGlzdG9yeS1vcmRlci1kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSwwQ0FBMEM7SUFDMUMsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsa0JBQWtCO0VBQ3BCOztFQUVBO0lBQ0UsV0FBVztJQUNYLGVBQWU7RUFDakI7O0VBRUE7SUFDRSxZQUFZO0lBQ1osVUFBVTtJQUNWLHFCQUFxQjtJQUNyQixZQUFZO0lBQ1osWUFBWTtJQUNaLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLFdBQVc7SUFDWCxlQUFlO0VBQ2pCOztFQUVBO0lBQ0UscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxZQUFZO0VBQ2QiLCJmaWxlIjoic3JjL2FwcC9oaXN0b3J5L2hpc3Rvcnktb3JkZXItZGV0YWlscy9oaXN0b3J5LW9yZGVyLWRldGFpbHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkIHtcclxuICAgIGJveC1zaGFkb3c6IDAgNHB4IDhweCAwIHJnYmEoMCwgMCwgMCwgMC4yKTtcclxuICAgIG1heC13aWR0aDogMzAwcHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LWZhbWlseTogYXJpYWw7XHJcbiAgfVxyXG4gIFxyXG4gIC50aXRsZSB7XHJcbiAgICBjb2xvcjogZ3JleTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICB9XHJcbiAgXHJcbiAgYnV0dG9uIHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG91dGxpbmU6IDA7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIGEge1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgZm9udC1zaXplOiAyMnB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gIH1cclxuICBcclxuICBidXR0b246aG92ZXIsIGE6aG92ZXIge1xyXG4gICAgb3BhY2l0eTogMC43O1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/history/history-order-details/history-order-details.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/history/history-order-details/history-order-details.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n      <img src=\"/assets/img/img.jpg\" alt=\"John\" style=\"width:100%\">\n      <h1>{{service.formData.FullName}}</h1>\n      <p class=\"title\">{{service.formData.Mobile}}</p>\n      <p>{{service.formData.EMPCode}}</p>\n      <p>{{service.formData.Balance}}</p>\n      <div style=\"margin: 24px 0;\">\n            <a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>\n            <a href=\"#\"><i class=\"fa fa-twitter ml-2\"></i></a>\n            <a href=\"#\"><i class=\"fa fa-linkedin ml-2\"></i></a>\n            <a href=\"#\"><i class=\"fa fa-facebook ml-2\"></i></a>\n            <p><button>Contact</button></p>\n      </div>\n</div>"

/***/ }),

/***/ "./src/app/history/history-order-details/history-order-details.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/history/history-order-details/history-order-details.component.ts ***!
  \**********************************************************************************/
/*! exports provided: HistoryOrderDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryOrderDetailsComponent", function() { return HistoryOrderDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var HistoryOrderDetailsComponent = /** @class */ (function () {
    function HistoryOrderDetailsComponent(service, router, currentRoute) {
        this.service = service;
        this.router = router;
        this.currentRoute = currentRoute;
    }
    HistoryOrderDetailsComponent.prototype.ngOnInit = function () {
        var empID = this.currentRoute.snapshot.paramMap.get('id');
        this.emp = this.service.getEmpployeeDetails(parseInt(empID));
        this.service.formData = Object.assign({}, this.emp);
        console.log(this.emp);
    };
    HistoryOrderDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-history-order-details',
            template: __webpack_require__(/*! ./history-order-details.component.html */ "./src/app/history/history-order-details/history-order-details.component.html"),
            styles: [__webpack_require__(/*! ./history-order-details.component.css */ "./src/app/history/history-order-details/history-order-details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_2__["EmployeeServiceService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], HistoryOrderDetailsComponent);
    return HistoryOrderDetailsComponent;
}());



/***/ }),

/***/ "./src/app/items/item-list/item-list.component.html":
/*!**********************************************************!*\
  !*** ./src/app/items/item-list/item-list.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-hover  mat-table\">\n  <tr>\n    <th>Image</th>\n    <th>Name</th>\n    <th>Price</th>\n    <th>Operation</th>\n  </tr>\n  <tr *ngFor=\"let item of service.list\">\n      <td ><img src=\"/Images/Items/{{item.Image}}\" height=\"50\" width=\"50\"></td>\n    <td >{{item.Name}}</td>\n    <td >{{item.Price}}</td>\n    <td >\n        <button (click)=\"populateForm(item)\" class=\"btn btn-sm btn-info text-white\"><i class=\"fa fa-pencil\"></i></button>\n        <button (click)=\"onDelete(item.ItemID)\" class=\"btn btn-sm btn-danger text-white ml-1\"><i class=\"fa fa-trash\"></i></button>\n    </td>\n  </tr>\n</table>"

/***/ }),

/***/ "./src/app/items/item-list/item-list.component.ts":
/*!********************************************************!*\
  !*** ./src/app/items/item-list/item-list.component.ts ***!
  \********************************************************/
/*! exports provided: ItemListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemListComponent", function() { return ItemListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/item.service */ "./src/app/shared/item.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");




var ItemListComponent = /** @class */ (function () {
    function ItemListComponent(service, toastr) {
        this.service = service;
        this.toastr = toastr;
    }
    ItemListComponent.prototype.ngOnInit = function () {
        this.service.refreshList();
    };
    ItemListComponent.prototype.populateForm = function (item) {
        this.service.formData = Object.assign({}, item);
    };
    ItemListComponent.prototype.onDelete = function (id) {
        var _this = this;
        if (confirm("Are you sure to delete this record?")) {
            this.service.deleteItem(id).subscribe(function (res) {
                _this.service.refreshList();
                _this.toastr.warning("Item Deleted succesfully", "Item Deleted");
            });
        }
    };
    ItemListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-item-list',
            template: __webpack_require__(/*! ./item-list.component.html */ "./src/app/items/item-list/item-list.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_2__["ItemService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], ItemListComponent);
    return ItemListComponent;
}());



/***/ }),

/***/ "./src/app/items/item/item.component.html":
/*!************************************************!*\
  !*** ./src/app/items/item/item.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #form=\"ngForm\" (submit)=\"onSubmit(form)\" autocomplete=\"off\">\n  <input type=\"hidden\" name=\"ItemID\" #ItemID=\"ngModel\" [(ngModel)]=\"service.formData.ItemID\">\n\n<div class=\"form-group\" ng-class=\"{'has-error': service.formData.Name.$touched && service.formData.Name.$error.required , 'has-success': service.formData.Name.$valid }\">\n  <label for=\"Name\" class=\" control-label\">Item Name</label>\n  <div>\n      <input name=\"Name\" #Name=\"ngModel\" [(ngModel)]=\"service.formData.Name\" class=\"form-control\" required>\n  </div>\n  <div>\n      <span class=\"help-block\" *ngIf=\"Name.invalid && Name.touched\" ng-show=\"service.formData.Name.$touched && service.formData.Name.$error.required\">Please enter Full Name.</span>\n  </div>\n</div>\n  <div class=\"form-group\">\n    <label>Price</label>\n    <input name=\"Price\" #Price=\"ngModel\" [(ngModel)]=\"service.formData.Price\" class=\"form-control\">\n  </div>\n  <div class=\"form-group\">\n    <input name=\"Image\"  type=\"file\" (change)=\"onFileSelected($event)\">\n  </div>\n  <div class=\"form-group\">\n    <button type=\"submit\" [disabled]=\"form.invalid\" class=\"btn btn-lg btn-block\">Submit</button>\n  </div>\n  \n</form>"

/***/ }),

/***/ "./src/app/items/item/item.component.ts":
/*!**********************************************!*\
  !*** ./src/app/items/item/item.component.ts ***!
  \**********************************************/
/*! exports provided: ItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemComponent", function() { return ItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/item.service */ "./src/app/shared/item.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");




var ItemComponent = /** @class */ (function () {
    function ItemComponent(service, toastr) {
        this.service = service;
        this.toastr = toastr;
    }
    ItemComponent.prototype.ngOnInit = function () {
        this.resetForm();
    };
    ItemComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.resetForm();
        this.service.formData = {
            ItemID: null,
            Name: '',
            Price: 0,
            Image: ''
        };
    };
    ItemComponent.prototype.onFileSelected = function (event) {
        this.selectedFile = new FormData();
        this.selectedFile = event.target.files[0];
        //console.log(this.selectedFile);
    };
    ItemComponent.prototype.onSubmit = function (form) {
        if (form.value.ItemID == null) {
            var formData = {
                Image: this.selectedFile,
                Name: form.value.Name,
                Price: form.value.Price
            };
            form.value.Image = formData.Image;
            form.value.Name = formData.Name;
            form.value.Price = parseFloat(formData.Price).toFixed(2);
            //console.log(form.value);
            this.insertRecord(form);
        }
        else {
            this.updateRecord(form);
        }
    };
    ItemComponent.prototype.insertRecord = function (form) {
        var _this = this;
        //form.value.Image=this.selectedFile;
        this.service.postItem(form.value).subscribe(function (res) {
            console.log(res);
            _this.toastr.success("Inserted Successfully", "Item Register");
            _this.resetForm(form);
            _this.service.refreshList();
        });
    };
    ItemComponent.prototype.updateRecord = function (form) {
        var _this = this;
        this.service.putItem(form.value).subscribe(function (res) {
            _this.toastr.info("Updated Successfully", "Item Updated");
            _this.resetForm(form);
            _this.service.refreshList();
        });
    };
    ItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-item',
            template: __webpack_require__(/*! ./item.component.html */ "./src/app/items/item/item.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_2__["ItemService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], ItemComponent);
    return ItemComponent;
}());



/***/ }),

/***/ "./src/app/items/items.component.css":
/*!*******************************************!*\
  !*** ./src/app/items/items.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2l0ZW1zL2l0ZW1zLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/items/items.component.html":
/*!********************************************!*\
  !*** ./src/app/items/items.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbtron\">\n  <h4 class=\"display-6 text-center\">Item Register</h4>\n  <hr>\n  <div class=\"row\">\n    <div class=\"col-md-5\">\n      <app-item></app-item>\n    </div>\n    <div class=\"col-md-7\">\n      <app-item-list></app-item-list>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/items/items.component.ts":
/*!******************************************!*\
  !*** ./src/app/items/items.component.ts ***!
  \******************************************/
/*! exports provided: ItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemsComponent", function() { return ItemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ItemsComponent = /** @class */ (function () {
    function ItemsComponent() {
    }
    ItemsComponent.prototype.ngOnInit = function () {
    };
    ItemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-items',
            template: __webpack_require__(/*! ./items.component.html */ "./src/app/items/items.component.html"),
            styles: [__webpack_require__(/*! ./items.component.css */ "./src/app/items/items.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ItemsComponent);
    return ItemsComponent;
}());



/***/ }),

/***/ "./src/app/navbar/navbar.component.html":
/*!**********************************************!*\
  !*** ./src/app/navbar/navbar.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-md bg-dark navbar-dark\" >\n  <!-- Brand -->\n  <a class=\"navbar-brand\" href=\"#\">\n    <img src=\"/assets/img/food.png\" width=\"50\" height=\"50\" style=\"width:40px;\">\n    InfinityHut Restaurant\n  </a>\n\n  <!-- Toggler/collapsibe Button -->\n  <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">\n    <span class=\"navbar-toggler-icon\"></span>\n  </button>\n\n  <!-- Navbar links -->\n  <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">\n    <ul class=\"navbar-nav\">\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/employees\" routerLinkActive=\"active\">Customers</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/user\" routerLinkActive=\"active\">User</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/orders\" routerLinkActive=\"active\">Orders</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/items\" routerLinkActive=\"active\">Items</a>\n      </li>\n      <li class=\"nav-item\">\n        <a class=\"nav-link\" routerLink=\"/history\" routerLinkActive=\"active\">History</a>\n      </li>\n      <li class=\"nav-item\">\n        <button class=\"btn btn-primary\" (click)=\"logoutme()\">Logout</button>\n      </li>\n    </ul>\n  </div> \n</nav>"

/***/ }),

/***/ "./src/app/navbar/navbar.component.ts":
/*!********************************************!*\
  !*** ./src/app/navbar/navbar.component.ts ***!
  \********************************************/
/*! exports provided: NavbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var NavbarComponent = /** @class */ (function () {
    function NavbarComponent(router) {
        this.router = router;
        this.title = 'EmployeeCRUD';
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.setActive = function (menu) {
        $('li').removeClass();
        $('#' + menu).addClass("active");
    };
    NavbarComponent.prototype.logoutme = function () {
        localStorage.removeItem('userToken');
        this.router.navigate(["/login"]);
    };
    NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__(/*! ./navbar.component.html */ "./src/app/navbar/navbar.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "./src/app/orders/order-items/order-items.component.html":
/*!***************************************************************!*\
  !*** ./src/app/orders/order-items/order-items.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4 class=\"display-4\">Order Food Item</h4>\n<form #form=\"ngForm\" autocomplete=\"off\" (submit)=\"onSubmit(form)\">\n    <input type=\"hidden\" name=\"OrderItemID\" #OrderItemID=\"ngModel\" [(ngModel)]=\"formData.OrderItemID\">\n    <input type=\"hidden\" name=\"OrderID\" #OrderID=\"ngModel\" [(ngModel)]=\"formData.OrderID\">\n    <input type=\"hidden\" name=\"ItemName\" #ItemName=\"ngModel\" [(ngModel)]=\"formData.ItemName\">\n  <div class=\"form-group\">\n    <label>Food Item</label>\n    <select name=\"ItemID\" #ItemID=\"ngModel\" [(ngModel)]=\"formData.ItemID\" class=\"form-control\" (change)=\"updatePrice($event.target)\" [class.is-invalid]=\"!isValid && formData.ItemID==0\">\n        <option value=\"0\">--select--</option>\n        <option *ngFor=\"let item of itemList\" value=\"{{item.ItemID}}\">{{item.Name}}</option>\n      </select>\n  </div>\n  <div class=\"form-row\"> \n\n  \n  <div class=\"form-group col-md-6\">\n      <label>Price</label>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\">\n          <div class=\"input-group-text\">$</div>\n        </div>\n        <input name=\"Price\" #Price=\"ngModel\" [(ngModel)]=\"formData.Price\" class=\"form-control\" readonly>\n      </div>\n      \n    </div>\n    <div class=\"form-group col-md-6\">\n        <label>Quantity</label>\n          <input name=\"Quantity\" #Quantity=\"ngModel\" [(ngModel)]=\"formData.Quantity\" class=\"form-control\" (keyup)=\"updateTotal()\" [class.is-invalid]=\"!isValid && formData.Quantity==0\">\n      </div>\n    </div>\n    <div class=\"form-group\">\n        <label>Grand Total</label>\n        <div class=\"input-group\">\n          <div class=\"input-group-prepend\">\n            <div class=\"input-group-text\">$</div>\n          </div>\n          <input name=\"Total\" #Total=\"ngModel\" [(ngModel)]=\"formData.Total\" class=\"form-control\" readonly>\n        </div>\n        \n      </div>\n      <div class=\"form-group\">\n          <button type=\"submit\" class=\"btn btn-dark\"><i class=\"fa fa-database\"></i> Submit</button>\n          <button type=\"button\" class=\"btn btn-outline-dark ml-1\" (click)=\"closeBox()\"><i class=\"fa fa-close\"></i> Close</button>\n        </div>\n</form>"

/***/ }),

/***/ "./src/app/orders/order-items/order-items.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/orders/order-items/order-items.component.ts ***!
  \*************************************************************/
/*! exports provided: OrderItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderItemsComponent", function() { return OrderItemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/item.service */ "./src/app/shared/item.service.ts");
/* harmony import */ var src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/order.service */ "./src/app/shared/order.service.ts");





var OrderItemsComponent = /** @class */ (function () {
    function OrderItemsComponent(data, dialogRef, itemService, orderService) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.itemService = itemService;
        this.orderService = orderService;
        this.isValid = true;
    }
    OrderItemsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.itemService.getItemList().then(function (res) { return _this.itemList = res; });
        if (this.data.orderItemIndex == null)
            this.formData = {
                OrderItemID: null,
                OrderID: this.data.OrderID,
                ItemID: 0,
                ItemName: '',
                Price: 0,
                Quantity: 0,
                Total: 0
            };
        else
            this.formData = Object.assign({}, this.orderService.orderItems[this.data.orderItemIndex]);
    };
    OrderItemsComponent.prototype.updatePrice = function (ctrl) {
        if (ctrl.selectedIndex == 0) {
            this.formData.Price = 0;
            this.formData.ItemName = '';
        }
        else {
            this.formData.Price = this.itemList[ctrl.selectedIndex - 1].Price;
            this.formData.ItemName = this.itemList[ctrl.selectedIndex - 1].Name;
            this.formData.Total = parseFloat((this.formData.Quantity * this.formData.Price).toFixed(2));
        }
    };
    OrderItemsComponent.prototype.updateTotal = function () {
        this.formData.Total = parseFloat((this.formData.Quantity * this.formData.Price).toFixed(2));
    };
    OrderItemsComponent.prototype.onSubmit = function (form) {
        if (this.validateForm(form.value) == true) {
            if (this.data.orderItemIndex == null)
                this.orderService.orderItems.push(form.value);
            else
                this.orderService.orderItems[this.data.orderItemIndex] = form.value;
            this.dialogRef.close();
        }
    };
    OrderItemsComponent.prototype.validateForm = function (formData) {
        this.isValid = true;
        if (formData.ItemID == 0)
            this.isValid = false;
        else if (formData.Quantity == 0)
            this.isValid = false;
        return this.isValid;
    };
    OrderItemsComponent.prototype.closeBox = function () {
        this.dialogRef.close();
    };
    OrderItemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-order-items',
            template: __webpack_require__(/*! ./order-items.component.html */ "./src/app/orders/order-items/order-items.component.html")
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            src_app_shared_item_service__WEBPACK_IMPORTED_MODULE_3__["ItemService"],
            src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"]])
    ], OrderItemsComponent);
    return OrderItemsComponent;
}());



/***/ }),

/***/ "./src/app/orders/order/order.component.css":
/*!**************************************************!*\
  !*** ./src/app/orders/order/order.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* You can add global styles to this file, and also import other style files */\r\ninput.ng-dirty.ng-invalid{\r\n    border-color: #dc3545;\r\n}\r\ndiv.validation-error{\r\n    width:100%;\r\n    margin-top: 25rem;\r\n    font-size: 80%;\r\n    color: #dc3545;\r\n}\r\nnav a:visited, a:link{\r\n    color: #607088;\r\n    \r\n}\r\nnav a:hover{\r\n    color: #039be5;\r\n    background-color: #cfd8dc;\r\n}\r\nnav a:active{\r\n    color: #039bef;\r\n    \r\n}\r\n.jumbotron{\r\n    padding-top: 0px;\r\n    background: #ffffff;\r\n}\r\n.form-control, .input-group-text{\r\n    border: 1px solid #0080ff;\r\n}\r\ntable .table-hover tbody tr:hover{\r\n    cursor: pointer;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXJzL29yZGVyL29yZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsOEVBQThFO0FBQzlFO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLGNBQWM7SUFDZCxjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxjQUFjOztBQUVsQjtBQUNBO0lBQ0ksY0FBYztJQUNkLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksY0FBYzs7QUFFbEI7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixtQkFBbUI7QUFDdkI7QUFFQTtJQUNJLHlCQUF5QjtBQUM3QjtBQUVBO0lBQ0ksZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL29yZGVycy9vcmRlci9vcmRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogWW91IGNhbiBhZGQgZ2xvYmFsIHN0eWxlcyB0byB0aGlzIGZpbGUsIGFuZCBhbHNvIGltcG9ydCBvdGhlciBzdHlsZSBmaWxlcyAqL1xyXG5pbnB1dC5uZy1kaXJ0eS5uZy1pbnZhbGlke1xyXG4gICAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xyXG59XHJcbmRpdi52YWxpZGF0aW9uLWVycm9ye1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIG1hcmdpbi10b3A6IDI1cmVtO1xyXG4gICAgZm9udC1zaXplOiA4MCU7XHJcbiAgICBjb2xvcjogI2RjMzU0NTtcclxufVxyXG5uYXYgYTp2aXNpdGVkLCBhOmxpbmt7XHJcbiAgICBjb2xvcjogIzYwNzA4ODtcclxuICAgIFxyXG59XHJcbm5hdiBhOmhvdmVye1xyXG4gICAgY29sb3I6ICMwMzliZTU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2ZkOGRjO1xyXG59XHJcbm5hdiBhOmFjdGl2ZXtcclxuICAgIGNvbG9yOiAjMDM5YmVmO1xyXG4gICAgXHJcbn1cclxuXHJcbi5qdW1ib3Ryb257XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxufVxyXG5cclxuLmZvcm0tY29udHJvbCwgLmlucHV0LWdyb3VwLXRleHR7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjMDA4MGZmO1xyXG59XHJcblxyXG50YWJsZSAudGFibGUtaG92ZXIgdGJvZHkgdHI6aG92ZXJ7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/orders/order/order.component.html":
/*!***************************************************!*\
  !*** ./src/app/orders/order/order.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-10 offset-1 text-center\">\n      <h4 class=\"display-6 text-center\">Create Order </h4>\n  </div>\n</div>\n<form #form=\"ngForm\" autocomplete=\"off\" *ngIf=\"service.formData\" (submit)=\"onSubmit(form)\">\n  <input type=\"hidden\" name=\"OrderID\" #OrderID=\"ngModel\" [(ngModel)]=\"service.formData.OrderID\">\n  <div class=\"row\" >\n      <div class=\"col-md-6\">\n        <div class=\"form-group\">\n          <label>Order No.</label>\n          <div class=\"input-group\">\n            <div class=\"input-group-prepend\">\n              <div class=\"input-group-text\">#</div>\n            </div>\n            <input name=\"OrderNo\" #OrderNo=\"ngModel\" [(ngModel)]=\"service.formData.OrderNo\" class=\"form-control\" readonly>\n          </div>\n        </div>\n        <div class=\"form-group\">\n          <label>Customer</label>\n          <select (change)=\"getBalance($event.target, lblName.id, customerBalance.id)\" name=\"EmployeeID\" #EmployeeID=\"ngModel\" [(ngModel)]=\"service.formData.EmployeeID\" class=\"form-control\"  [class.is-invalid]=\"!isValid && service.formData.EmployeeID==0\">\n            <option value=\"0\">--select--</option>\n            <option *ngFor=\"let item of customerList\" value=\"{{item.EmployeeID}}\">{{item.FullName}}</option>\n          </select>\n        </div>\n\n\n\n        <div class=\"form-group\">\n            <label>Customer Balance.</label>\n            <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                <div  #lblName id=\"lblName\" class=\"input-group-text\">{{labelBalance}}</div>\n              </div>\n              <!-- <div  #customerBalance id=\"customerBalance\" class=\"input-group-text\">#</div> -->\n              <input  #customerBalance id=\"customerBalance\" class=\"form-control\" value=\"{{userBalance}}\"  readonly>\n            </div>\n          </div>\n      </div>\n      <div class=\"col-md-6\">\n          <div class=\"form-group\">\n            <label>Payment Method</label>\n            <select name=\"PMethod\" #PMethod=\"ngModel\" [(ngModel)]=\"service.formData.PMethod\" class=\"form-control\">\n                <option value=\"\">--select--</option>\n                <option value=\"Cash\">Cash</option>\n                <option value=\"Card\">Card</option>\n            </select>\n          </div>\n          <div class=\"form-group\">\n            <label>Grand Total</label>\n            <div class=\"input-group\">\n              <div class=\"input-group-prepend\">\n                <div class=\"input-group-text\">$</div>\n              </div>\n              <input name=\"GTotal\" #GTotal=\"ngModel\" [(ngModel)]=\"service.formData.GTotal\" class=\"form-control\" readonly>\n            </div>\n          </div>\n      </div>\n  </div>\n  <!--order items table-->\n  <table class=\"table  mat-table\">\n    <thead class=\"thead-light\">\n        <th>Food</th>\n        <th>Price</th>\n        <th>Quantity</th>\n        <th>Total</th>\n        <th><a class=\"btn btn-sm btn-success text-white\" (click)=\"AddOrEditOrderItem(null, service.formData.OrderID)\"><i class=\"fa fa-plus\"></i> Add Item</a></th>\n    </thead>\n    <tbody>\n      <tr *ngIf=\"service.orderItems.length==0\" [class.text-danger]=\"!isValid && service.orderItems.length==0\">\n        <td class=\"font-italic text-center\" colspan=\"100%\">\n          No Food Item Selected for this order\n        </td>\n      </tr>\n      <tr *ngFor=\"let item of service.orderItems;let i=index;\">\n          <td>{{item.ItemName}}</td>\n          <td>{{item.Price}}</td>\n          <td>{{item.Quantity}}</td>\n          <td>{{item.Total}}</td>\n          <td>\n              <a class=\"btn btn-sm btn-info text-white\" (click)=\"AddOrEditOrderItem(i, service.formData.OrderID)\"><i class=\"fa fa-pencil\"></i></a>\n              <a class=\"btn btn-sm btn-danger text-white ml-1\" (click)=\"onDeleteOrderItem(item.OrderItemID, i)\"><i class=\"fa fa-trash\"></i></a>\n          </td>\n      </tr>\n    </tbody>\n  </table>\n  <div class=\"form-group\">\n    <button type=\"submit\" class=\"btn btn-dark\"><i class=\"fa fa-database\"></i> Submit</button>\n    <a class=\"btn btn-outline-dark ml-1\" routerLink=\"/orders\" ><i class=\"fa fa-table\"></i>  View Orders</a>\n  </div>\n</form>\n"

/***/ }),

/***/ "./src/app/orders/order/order.component.ts":
/*!*************************************************!*\
  !*** ./src/app/orders/order/order.component.ts ***!
  \*************************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/order.service */ "./src/app/shared/order.service.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _order_items_order_items_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../order-items/order-items.component */ "./src/app/orders/order-items/order-items.component.ts");
/* harmony import */ var src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/shared/employee.service.service */ "./src/app/shared/employee.service.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var OrderComponent = /** @class */ (function () {
    function OrderComponent(service, dialog, customerService, toastr, router, currentRoute) {
        this.service = service;
        this.dialog = dialog;
        this.customerService = customerService;
        this.toastr = toastr;
        this.router = router;
        this.currentRoute = currentRoute;
        this.isValid = true;
    }
    OrderComponent.prototype.ngOnInit = function () {
        var _this = this;
        var orderID = this.currentRoute.snapshot.paramMap.get('id');
        if (orderID == null)
            this.resetForm();
        else {
            this.service.getOrderByID(parseInt(orderID)).then(function (res) {
                //console.log(res);
                _this.service.formData = res.order;
                _this.service.orderItems = res.item;
                _this.service.formData.DeletedOrderItemIDs = "";
                _this.userBalance = res.order['Balance'];
                _this.labelString = _this.getLabel(res.order['Status']);
                _this.labelBalance = _this.labelString;
            });
        }
        this.customerService.getCustomerList().then(function (res) { _this.customerList = res; });
    };
    OrderComponent.prototype.resetForm = function (form) {
        if (form = null)
            form.resetForm();
        this.service.formData = {
            OrderID: null,
            OrderNo: Math.floor(100000 + Math.random() * 900000).toString(),
            EmployeeID: 0,
            PMethod: '',
            GTotal: 0,
            Status: 0,
            DeletedOrderItemIDs: ''
        };
        this.service.orderItems = [];
    };
    OrderComponent.prototype.AddOrEditOrderItem = function (orderItemIndex, OrderID) {
        var _this = this;
        var dialogConfig = new _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialogConfig"]();
        dialogConfig.autoFocus = true;
        dialogConfig.disableClose = true;
        dialogConfig.width = "50%";
        dialogConfig.data = { orderItemIndex: orderItemIndex, OrderID: OrderID };
        this.dialog.open(_order_items_order_items_component__WEBPACK_IMPORTED_MODULE_4__["OrderItemsComponent"], dialogConfig).afterClosed().subscribe(function (res) {
            _this.updateGrandTotal();
        });
    };
    OrderComponent.prototype.onDeleteOrderItem = function (orderItemID, i) {
        if (orderItemID != null)
            this.service.formData.DeletedOrderItemIDs += orderItemID + ",";
        this.service.orderItems.splice(i, 1);
        this.updateGrandTotal();
    };
    OrderComponent.prototype.updateGrandTotal = function () {
        this.service.formData.GTotal = this.service.orderItems.reduce(function (prev, curr) {
            return prev + curr.Total;
        }, 0);
        this.service.formData.GTotal = parseFloat(this.service.formData.GTotal.toFixed(2));
    };
    OrderComponent.prototype.validateForm = function () {
        this.isValid = true;
        if (this.service.formData.EmployeeID == 0)
            this.isValid = false;
        else if (this.service.orderItems.length == 0)
            this.isValid = false;
        return this.isValid;
    };
    OrderComponent.prototype.onSubmit = function (form) {
        var _this = this;
        if (this.validateForm()) {
            //console.log(this.service.formData);
            // console.log(this.service.orderItems);
            this.service.saveorUpdateOrder().subscribe(function (res) {
                console.log(res);
                _this.resetForm();
                _this.toastr.success("Inserted Successfully", "Restaurant App");
                _this.router.navigate(['/orders']);
            });
        }
    };
    OrderComponent.prototype.getBalance = function (ctrl, lblName, customerBalance) {
        if (ctrl.selectedIndex == 0) {
            this.str = "0.00";
            this.labelString = "No Balance";
        }
        else {
            this.labelString = this.getLabel(this.customerList[ctrl.selectedIndex - 1].Status);
            this.str = this.customerList[ctrl.selectedIndex - 1].Balance;
        }
        this.userBalance = this.str;
        this.labelBalance = this.labelString;
    };
    OrderComponent.prototype.getLabel = function (status) {
        switch (status) {
            case "1":
                return this.labelString = "Advance";
                break;
            case "2":
                return this.labelString = "Previous Balance";
            default:
                return this.labelString = "No Balance";
                break;
        }
    };
    OrderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-order',
            template: __webpack_require__(/*! ./order.component.html */ "./src/app/orders/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/orders/order/order.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_3__["MatDialog"],
            src_app_shared_employee_service_service__WEBPACK_IMPORTED_MODULE_5__["EmployeeServiceService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_6__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/orders/orders.component.html":
/*!**********************************************!*\
  !*** ./src/app/orders/orders.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-md-10 offset-1 text-center\">\n        <h4 class=\"display-6 text-center\">Orders</h4>\n    </div>\n  </div>\n<table class=\"table table-hover mat-table\">\n  <thead class=\"thead-light\">\n    <th>#Order No</th>\n    <th>Customer</th>\n    <th>P. Method</th>\n    <th>G. Total</th>\n    <th>\n      <a class=\"btn btn-outline-success\" routerLink=\"/order\" ><i class=\"fa fa-plus-square\"></i>  Create New</a>\n    </th>\n  </thead>\n  <tbody>\n      \n    <tr *ngFor=\"let item of orderList\">\n      <th (click)=\"openForEdit(item.OrderID)\">{{item.OrderNo}}</th>\n      <td >{{item.employee['FullName']}}</td>\n      <td >{{item.PMethod}}</td>\n      <td >{{item.GTotal}}</td>\n      <td>\n        <button (click)=\"openForEdit(item.OrderID)\" class=\"btn btn-sm btn-info text-white\"><i class=\"fa fa-pencil\"></i></button>\n        <a class=\"btn btn-sm btn-danger text-white ml-2\" (click)=\"onDeleteOrder(item.OrderID, i)\"><i class=\"fa fa-trash\"></i></a>\n      </td>\n    </tr>\n  </tbody>\n</table>\n"

/***/ }),

/***/ "./src/app/orders/orders.component.ts":
/*!********************************************!*\
  !*** ./src/app/orders/orders.component.ts ***!
  \********************************************/
/*! exports provided: OrdersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersComponent", function() { return OrdersComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/order.service */ "./src/app/shared/order.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");





var OrdersComponent = /** @class */ (function () {
    function OrdersComponent(service, router, toastr) {
        this.service = service;
        this.router = router;
        this.toastr = toastr;
    }
    OrdersComponent.prototype.ngOnInit = function () {
        this.refreshList();
    };
    OrdersComponent.prototype.refreshList = function () {
        var _this = this;
        this.service.getOrderList().then(function (res) { return _this.orderList = res; });
    };
    OrdersComponent.prototype.openForEdit = function (OrderID) {
        this.router.navigate(['/order/edit/' + OrderID]);
    };
    OrdersComponent.prototype.onDeleteOrder = function (OrderID) {
        var _this = this;
        if (confirm("Are you sure to delete this record")) {
            this.service.deleteOrder(OrderID).then(function (res) {
                _this.refreshList();
                _this.toastr.warning("Deleted Successfully", "Restaurant App");
            });
        }
    };
    OrdersComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-orders',
            template: __webpack_require__(/*! ./orders.component.html */ "./src/app/orders/orders.component.html"),
            styles: ["./orders.component.css"]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_shared_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_4__["ToastrService"]])
    ], OrdersComponent);
    return OrdersComponent;
}());



/***/ }),

/***/ "./src/app/orders/print-layout/print-layout.component.css":
/*!****************************************************************!*\
  !*** ./src/app/orders/print-layout/print-layout.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header, .header-space, .footer, .footer-space { height: 100px; }\r\n.header { position: fixed; top: 0; }\r\n.footer { position: fixed; bottom: 0; }\r\n@media screen {\r\n  :host {\r\n    display: none;\r\n  }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXJzL3ByaW50LWxheW91dC9wcmludC1sYXlvdXQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxpREFBaUQsYUFBYSxFQUFFO0FBQ2hFLFVBQVUsZUFBZSxFQUFFLE1BQU0sRUFBRTtBQUNuQyxVQUFVLGVBQWUsRUFBRSxTQUFTLEVBQUU7QUFDdEM7RUFDRTtJQUNFLGFBQWE7RUFDZjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvb3JkZXJzL3ByaW50LWxheW91dC9wcmludC1sYXlvdXQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXIsIC5oZWFkZXItc3BhY2UsIC5mb290ZXIsIC5mb290ZXItc3BhY2UgeyBoZWlnaHQ6IDEwMHB4OyB9XHJcbi5oZWFkZXIgeyBwb3NpdGlvbjogZml4ZWQ7IHRvcDogMDsgfVxyXG4uZm9vdGVyIHsgcG9zaXRpb246IGZpeGVkOyBib3R0b206IDA7IH1cclxuQG1lZGlhIHNjcmVlbiB7XHJcbiAgOmhvc3Qge1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxuICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/orders/print-layout/print-layout.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/orders/print-layout/print-layout.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table>\n    <thead><tr><td>\n      <div class=\"header-space\">&nbsp;</div>\n    </td></tr></thead>\n    <tbody><tr><td>\n      <div class=\"content\">\n        <router-outlet></router-outlet>\n      </div>\n    </td></tr></tbody>\n    <tfoot><tr><td>\n      <div class=\"footer-space\">&nbsp;</div>\n    </td></tr></tfoot>\n  </table>\n  \n  <div class=\"header\">COOL HEADER</div>\n  <div class=\"footer\">AWESOME FOOTER</div>"

/***/ }),

/***/ "./src/app/orders/print-layout/print-layout.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/orders/print-layout/print-layout.component.ts ***!
  \***************************************************************/
/*! exports provided: PrintLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintLayoutComponent", function() { return PrintLayoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PrintLayoutComponent = /** @class */ (function () {
    function PrintLayoutComponent() {
    }
    PrintLayoutComponent.prototype.ngOnInit = function () {
    };
    PrintLayoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-print-layout',
            template: __webpack_require__(/*! ./print-layout.component.html */ "./src/app/orders/print-layout/print-layout.component.html"),
            styles: [__webpack_require__(/*! ./print-layout.component.css */ "./src/app/orders/print-layout/print-layout.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PrintLayoutComponent);
    return PrintLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/employee.service.service.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/employee.service.service.ts ***!
  \****************************************************/
/*! exports provided: EmployeeServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeServiceService", function() { return EmployeeServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var EmployeeServiceService = /** @class */ (function () {
    function EmployeeServiceService(http) {
        this.http = http;
        this.rootURL = "http://empployee.local/api";
    }
    EmployeeServiceService.prototype.postEmployee = function (formData) {
        return this.http.post(this.rootURL + "/storeEmployees", formData);
    };
    EmployeeServiceService.prototype.refreshList = function () {
        var _this = this;
        this.http.get(this.rootURL + "/employeesList")
            .toPromise().then(function (res) { return _this.list = res; });
    };
    EmployeeServiceService.prototype.putEmployee = function (formData) {
        return this.http.put(this.rootURL + "/updateEmployee", formData);
    };
    EmployeeServiceService.prototype.deleteEmpployee = function (id) {
        return this.http.delete(this.rootURL + "/deleteEmployee/" + id);
    };
    EmployeeServiceService.prototype.getCustomerList = function () {
        return this.http.get(this.rootURL + "/employeesList").toPromise();
    };
    EmployeeServiceService.prototype.getEmpployeeDetails = function (id) {
        var _this = this;
        return this.http.get(this.rootURL + "/showEmployee/" + id).toPromise().then(function (res) { return _this.formData = res; });
    };
    EmployeeServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], EmployeeServiceService);
    return EmployeeServiceService;
}());



/***/ }),

/***/ "./src/app/shared/item.service.ts":
/*!****************************************!*\
  !*** ./src/app/shared/item.service.ts ***!
  \****************************************/
/*! exports provided: ItemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemService", function() { return ItemService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var ItemService = /** @class */ (function () {
    function ItemService(http) {
        this.http = http;
    }
    ItemService.prototype.getItemList = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/itemsList").toPromise();
    };
    ItemService.prototype.refreshList = function () {
        var _this = this;
        this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/itemsList")
            .toPromise().then(function (res) { return _this.list = res; });
    };
    ItemService.prototype.postItem = function (formData) {
        var myFormData = new FormData();
        var price = formData.Price.toString();
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        myFormData.append('Image', formData.Image);
        myFormData.append('Name', formData.Name);
        myFormData.append("Price", price);
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/storeItem", myFormData, { headers: headers });
    };
    ItemService.prototype.putItem = function (formData) {
        return this.http.put(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/updateItem", formData);
    };
    ItemService.prototype.deleteItem = function (id) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/deleteItem/" + id);
    };
    ItemService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ItemService);
    return ItemService;
}());



/***/ }),

/***/ "./src/app/shared/order.service.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/order.service.ts ***!
  \*****************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
    }
    OrderService.prototype.saveorUpdateOrder = function () {
        var body = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.formData, { OrderItems: this.orderItems });
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/orders", body);
    };
    OrderService.prototype.getOrderList = function () {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/orders").toPromise();
    };
    OrderService.prototype.getOrderByID = function (id) {
        return this.http.get(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/orders/" + id).toPromise();
    };
    OrderService.prototype.deleteOrder = function (orderID) {
        return this.http.delete(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/destroyorder/" + orderID).toPromise();
    };
    OrderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], OrderService);
    return OrderService;
}());



/***/ }),

/***/ "./src/app/shared/user.service.ts":
/*!****************************************!*\
  !*** ./src/app/shared/user.service.ts ***!
  \****************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.userAuthentication = function (userName, password) {
        var data = { "username": userName, "password": password };
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        return this.http.post(src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].apiURL + "/login", data, { headers: headers });
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.css":
/*!****************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvc2lnbi1pbi9zaWduLWluLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.html":
/*!*****************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"isLoginError\" class=\"red-text center error-message\">Incorrect UserName or Password</div>\n<form #form=\"ngForm\" (submit)=\"onSubmit(email.value, password.value)\" autocomplete=\"off\">\n  <div class=\"form-group\"\n    ng-class=\"{'has-error': UserService.formData.email.$touched && UserService.formData.email.$error.required , 'has-success': UserService.formData.email.$valid }\">\n    <label for=\"email\" class=\" control-label\"><i class=\"fa fa-user-circle\"></i></label>\n    \n    <div>\n      <input name=\"email\" #email=\"ngModel\" [(ngModel)]=\"UserService.formData.email\" class=\"form-control\" required placeholder=\"Email\">\n    </div>\n    <div>\n      <span class=\"help-block\" *ngIf=\"email.invalid && email.touched\"\n        ng-show=\"UserService.formData.email.$touched && UserService.formData.email.$error.required\">Please enter Email.</span>\n    </div>\n  </div>\n  <div class=\"form-group\"\n    ng-class=\"{'has-error': UserService.formData.password.$touched && UserService.formData.password.$error.required , 'has-success': UserService.formData.password.$valid }\">\n    <label for=\"password\" class=\" control-label\"><i class=\"fa fa-fw  fa-eye\"></i></label>\n    <div>\n      <input type=\"password\" name=\"password\" #password=\"ngModel\" [(ngModel)]=\"UserService.formData.password\"\n        class=\"form-control\" required placeholder=\"Password\">\n    </div>\n    <div>\n      <span class=\"help-block\" *ngIf=\"password.invalid && password.touched\"\n        ng-show=\"UserService.formData.password.$touched && UserService.formData.password.$error.required\">Please enter\n        Password.</span>\n    </div>\n  </div>\n  <div class=\"form-group\">\n    <button type=\"submit\" [disabled]=\"form.invalid\" class=\"btn btn-lg btn-block\">Login</button>\n  </div>\n</form>"

/***/ }),

/***/ "./src/app/user/sign-in/sign-in.component.ts":
/*!***************************************************!*\
  !*** ./src/app/user/sign-in/sign-in.component.ts ***!
  \***************************************************/
/*! exports provided: SignInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInComponent", function() { return SignInComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/user.service */ "./src/app/shared/user.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var SignInComponent = /** @class */ (function () {
    function SignInComponent(UserService, toastr, router) {
        this.UserService = UserService;
        this.toastr = toastr;
        this.router = router;
        this.isLoginError = false;
    }
    SignInComponent.prototype.ngOnInit = function () {
        this.resetForm();
    };
    SignInComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.resetForm();
        this.UserService.formData = {
            id: null,
            name: '',
            email: '',
            password: ''
        };
    };
    SignInComponent.prototype.onSubmit = function (UserName, Password) {
        var _this = this;
        this.UserService.userAuthentication(UserName, Password).subscribe(function (data) {
            localStorage.setItem('userToken', data.access_token);
            _this.router.navigate(["/employees"]);
        }, function (err) {
            _this.isLoginError = true;
        });
    };
    SignInComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-in',
            template: __webpack_require__(/*! ./sign-in.component.html */ "./src/app/user/sign-in/sign-in.component.html"),
            styles: [__webpack_require__(/*! ./sign-in.component.css */ "./src/app/user/sign-in/sign-in.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], SignInComponent);
    return SignInComponent;
}());



/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.css":
/*!****************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvc2lnbi11cC9zaWduLXVwLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.html":
/*!*****************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #form=\"ngForm\" (submit)=\"onSubmit(form)\" autocomplete=\"off\">\n    <div class=\"form-group\"\n      ng-class=\"{'has-error': UserService.formData.name.$touched && UserService.formData.name.$error.required , 'has-success': UserService.formData.name.$valid }\">\n      <label for=\"name\" class=\" control-label\">Full Name</label>\n      <div>\n        <input name=\"name\" #name=\"ngModel\" [(ngModel)]=\"UserService.formData.name\" class=\"form-control\" required>\n      </div>\n      <div>\n        <span class=\"help-block\" *ngIf=\"name.invalid && name.touched\"\n          ng-show=\"UserService.formData.name.$touched && UserService.formData.name.$error.required\">Please enter Full Name.</span>\n      </div>\n    </div>\n  \n    <div class=\"form-group\"\n      ng-class=\"{'has-error': UserService.formData.email.$touched && UserService.formData.email.$error.required , 'has-success': UserService.formData.email.$valid }\">\n      <label for=\"email\" class=\" control-label\">Email</label>\n      <div>\n        <input name=\"email\" #email=\"ngModel\" [(ngModel)]=\"UserService.formData.email\" class=\"form-control\" required>\n      </div>\n      <div>\n        <span class=\"help-block\" *ngIf=\"email.invalid && email.touched\"\n          ng-show=\"UserService.formData.email.$touched && UserService.formData.email.$error.required\">Please enter Email.</span>\n      </div>\n    </div>\n  \n    <div class=\"form-group\"\n      ng-class=\"{'has-error': UserService.formData.password.$touched && UserService.formData.password.$error.required , 'has-success': UserService.formData.password.$valid }\">\n      <label for=\"password\" class=\" control-label\">Password</label>\n      <div>\n        <input type=\"password\" name=\"password\" #password=\"ngModel\" [(ngModel)]=\"UserService.formData.password\"\n          class=\"form-control\" required>\n      </div>\n      <div>\n        <span class=\"help-block\" *ngIf=\"password.invalid && password.touched\"\n          ng-show=\"UserService.formData.password.$touched && UserService.formData.password.$error.required\">Please enter\n          Password.</span>\n      </div>\n    </div>\n    <div class=\"form-group\">\n      <button type=\"submit\" [disabled]=\"form.invalid\" class=\"btn btn-lg btn-block\">SignUp</button>\n    </div>\n  </form>"

/***/ }),

/***/ "./src/app/user/sign-up/sign-up.component.ts":
/*!***************************************************!*\
  !*** ./src/app/user/sign-up/sign-up.component.ts ***!
  \***************************************************/
/*! exports provided: SignUpComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpComponent", function() { return SignUpComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_shared_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/user.service */ "./src/app/shared/user.service.ts");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");




var SignUpComponent = /** @class */ (function () {
    function SignUpComponent(UserService, toastr) {
        this.UserService = UserService;
        this.toastr = toastr;
    }
    SignUpComponent.prototype.ngOnInit = function () {
        this.resetForm();
    };
    SignUpComponent.prototype.resetForm = function (form) {
        if (form != null)
            form.resetForm();
        this.UserService.formData = {
            id: null,
            name: '',
            email: '',
            password: ''
        };
    };
    SignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! ./sign-up.component.html */ "./src/app/user/sign-up/sign-up.component.html"),
            styles: [__webpack_require__(/*! ./sign-up.component.css */ "./src/app/user/sign-up/sign-up.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_shared_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"],
            ngx_toastr__WEBPACK_IMPORTED_MODULE_3__["ToastrService"]])
    ], SignUpComponent);
    return SignUpComponent;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-tab-group {\r\n    margin-bottom: 48px;\r\n  }\r\n\r\n  /* You can add global styles to this file, and also import other style files */\r\n\r\n  input.ng-dirty.ng-invalid{\r\n  border-color: #dc3545;\r\n}\r\n\r\n  div.validation-error{\r\n  width:100%;\r\n  margin-top: 25rem;\r\n  font-size: 80%;\r\n  color: #dc3545;\r\n}\r\n\r\n  nav a:visited, a:link{\r\n  color: #607088;\r\n  \r\n}\r\n\r\n  nav a:hover{\r\n  color: #039be5;\r\n  background-color: #cfd8dc;\r\n}\r\n\r\n  nav a:active{\r\n  color: #039bef;\r\n  \r\n}\r\n\r\n  .jumbotron{\r\n  padding-top: 0px;\r\n  background: #ffffff;\r\n}\r\n\r\n  .form-control, .input-group-text{\r\n  border: 1px solid #0080ff;\r\n}\r\n\r\n  table .table-hover tbody tr:hover{\r\n  cursor: pointer;\r\n}\r\n\r\n  .mat-radio-button ~ .mat-radio-button {\r\n  margin-left: 16px;\r\n}\r\n\r\n  i.fa {\r\n  display: inline-block;\r\n  border-radius: 60px;\r\n  box-shadow: 0px 0px 2px #888;\r\n  padding: 0.5em 0.6em;\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci91c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7RUFDckI7O0VBRUEsOEVBQThFOztFQUNoRjtFQUNFLHFCQUFxQjtBQUN2Qjs7RUFDQTtFQUNFLFVBQVU7RUFDVixpQkFBaUI7RUFDakIsY0FBYztFQUNkLGNBQWM7QUFDaEI7O0VBQ0E7RUFDRSxjQUFjOztBQUVoQjs7RUFDQTtFQUNFLGNBQWM7RUFDZCx5QkFBeUI7QUFDM0I7O0VBQ0E7RUFDRSxjQUFjOztBQUVoQjs7RUFFQTtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7QUFDckI7O0VBRUE7RUFDRSx5QkFBeUI7QUFDM0I7O0VBRUE7RUFDRSxlQUFlO0FBQ2pCOztFQUNBO0VBQ0UsaUJBQWlCO0FBQ25COztFQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQiw0QkFBNEI7RUFDNUIsb0JBQW9COztBQUV0QiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdXNlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC10YWItZ3JvdXAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNDhweDtcclxuICB9XHJcblxyXG4gIC8qIFlvdSBjYW4gYWRkIGdsb2JhbCBzdHlsZXMgdG8gdGhpcyBmaWxlLCBhbmQgYWxzbyBpbXBvcnQgb3RoZXIgc3R5bGUgZmlsZXMgKi9cclxuaW5wdXQubmctZGlydHkubmctaW52YWxpZHtcclxuICBib3JkZXItY29sb3I6ICNkYzM1NDU7XHJcbn1cclxuZGl2LnZhbGlkYXRpb24tZXJyb3J7XHJcbiAgd2lkdGg6MTAwJTtcclxuICBtYXJnaW4tdG9wOiAyNXJlbTtcclxuICBmb250LXNpemU6IDgwJTtcclxuICBjb2xvcjogI2RjMzU0NTtcclxufVxyXG5uYXYgYTp2aXNpdGVkLCBhOmxpbmt7XHJcbiAgY29sb3I6ICM2MDcwODg7XHJcbiAgXHJcbn1cclxubmF2IGE6aG92ZXJ7XHJcbiAgY29sb3I6ICMwMzliZTU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NmZDhkYztcclxufVxyXG5uYXYgYTphY3RpdmV7XHJcbiAgY29sb3I6ICMwMzliZWY7XHJcbiAgXHJcbn1cclxuXHJcbi5qdW1ib3Ryb257XHJcbiAgcGFkZGluZy10b3A6IDBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG59XHJcblxyXG4uZm9ybS1jb250cm9sLCAuaW5wdXQtZ3JvdXAtdGV4dHtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjMDA4MGZmO1xyXG59XHJcblxyXG50YWJsZSAudGFibGUtaG92ZXIgdGJvZHkgdHI6aG92ZXJ7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5tYXQtcmFkaW8tYnV0dG9uIH4gLm1hdC1yYWRpby1idXR0b24ge1xyXG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xyXG59XHJcblxyXG5pLmZhIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcclxuICBib3gtc2hhZG93OiAwcHggMHB4IDJweCAjODg4O1xyXG4gIHBhZGRpbmc6IDAuNWVtIDAuNmVtO1xyXG5cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h3>No animation</h3>\n\n<mat-tab-group animationDuration=\"0ms\">\n  <mat-tab label=\"Sign In\"><app-sign-in></app-sign-in></mat-tab>\n  <mat-tab label=\"Sign Up\"><app-sign-up></app-sign-up></mat-tab>\n</mat-tab-group>\n\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    apiURL: 'http://empployee.local/api'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! F:\wamp\www\EmployeeRecords\ngAPI\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map