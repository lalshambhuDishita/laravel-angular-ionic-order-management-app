<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/emp', 'EmployeeController@index')->name('employee');

Route::post('/api/storeEmployees', 'EmployeeController@store');
Route::put('/api/updateEmployee', 'EmployeeController@updateEmployee');
Route::delete('/api/deleteEmployee/{id}', 'EmployeeController@destroy');
Route::get('/api/employeesList', 'EmployeeController@index');
Route::get('/api/showEmployee/{id}', 'EmployeeController@show');


Route::post('/api/storeItem', 'ItemController@store');
Route::put('/api/updateItem', 'ItemController@updateItem');
Route::delete('/api/deleteItem/{id}', 'ItemController@destroy');
Route::get('/api/itemsList', 'ItemController@getItems');

Route::resource('/api/orders', 'OrderController');
Route::delete('/api/destroyorder/{id}', 'OrderController@destroyorder');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/customer', 'CustomerController@index')->name('customer');

