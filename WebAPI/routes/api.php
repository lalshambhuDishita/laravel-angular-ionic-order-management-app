<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::post('/registerCustomer', 'AuthController@registerCustomer');
Route::middleware('auth:api')->post('/logout', 'AuthController@logout');

Route::post('/emplogin', 'AuthemployeeController@login');
Route::post('/empregister', 'AuthemployeeController@register');
Route::post('/registerEmployee', 'AuthemployeeController@registerEmployee');
Route::middleware('auth:api')->post('/logout', 'AuthemployeeController@logout');