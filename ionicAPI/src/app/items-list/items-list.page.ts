import { Component, OnInit} from '@angular/core';
import { ItemsService  } from "../shared/items.service";
import { Items } from "../models/items";
import { NavController } from '@ionic/angular';





@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.page.html',
  styleUrls: ['./items-list.page.scss'],
})
export class ItemsListPage implements OnInit {

  itemList : Items[];

  constructor(
    private itemService: ItemsService,
    private navCtrl: NavController
    ) { }

  ngOnInit() {
    this.itemService.getItemList().then(
      res => this.itemList = res as Items[]
      ); 
  }
  public gotoItems(){
    this.navCtrl.navigateForward('items');
  }

 

}
