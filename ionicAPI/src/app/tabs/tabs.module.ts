import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ItemsPageModule } from "../items/items.module";
import { ItemsListPageModule } from "../items-list/items-list.module";

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    ItemsPageModule,
    ItemsListPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
