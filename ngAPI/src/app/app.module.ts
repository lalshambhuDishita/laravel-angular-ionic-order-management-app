import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule  } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatDialogModule} from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatRadioModule} from '@angular/material/radio';
import {MatTabsModule} from '@angular/material/tabs';

import { AppComponent } from './app.component';
import { EmployeesComponent } from './employees/employees/employees.component';
import { EmployeeComponent } from './employees/employees/employee/employee.component';
import { EmployeeListComponent } from './employees/employees/employee-list/employee-list.component';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { AppRoutingModule } from './app-routing.module';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';
import { OrderItemsComponent } from './orders/order-items/order-items.component';
import { OrderService } from './shared/order.service';
import { UserService } from "./shared/user.service";
import { NavbarComponent } from './navbar/navbar.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './items/item/item.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import { PrintLayoutComponent } from './orders/print-layout/print-layout.component';
import { HistoryEmployeeOrderComponent } from './history/history-employee-order/history-employee-order.component';
import { HistoryOrderDetailsComponent } from './history/history-order-details/history-order-details.component';
import { UserComponent } from './user/user.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';

import { AuthGuard } from "./auth/auth.guard";

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    EmployeeComponent,
    EmployeeListComponent,
    OrdersComponent,
    OrderComponent,
    OrderItemsComponent,
    NavbarComponent,
    ItemsComponent,
    ItemComponent,
    ItemListComponent,
    PrintLayoutComponent,
    HistoryEmployeeOrderComponent,
    HistoryOrderDetailsComponent,
    UserComponent,
    SignInComponent,
    SignUpComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    MatDialogModule,
    AngularFontAwesomeModule,
    MatRadioModule,
    MatTabsModule
  ],
  entryComponents:[OrderItemsComponent],
  providers: [EmployeeServiceService, OrderService, UserService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
