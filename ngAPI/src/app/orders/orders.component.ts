import { Component, OnInit } from '@angular/core';
import { OrderService } from '../shared/order.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {MatTableModule} from '@angular/material/table';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styles: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orderList;

  constructor(private service : OrderService,
    private router:Router,
    private toastr:ToastrService) { }

  ngOnInit() {
    this.refreshList();
  }

  refreshList(){
    this.service.getOrderList().then(res => this.orderList=res);
  }

  openForEdit(OrderID : number){
    this.router.navigate(['/order/edit/'+OrderID]);

  }
  onDeleteOrder(OrderID : number){
    if(confirm("Are you sure to delete this record")){
      this.service.deleteOrder(OrderID).then(res=>{
        this.refreshList();
        this.toastr.warning("Deleted Successfully", "Restaurant App");
      })
    }
    
  }

}
