import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { OrderService } from 'src/app/shared/order.service';
import { NgForm } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OrderItemsComponent } from '../order-items/order-items.component';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { Employee } from 'src/app/shared/employee.model';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import {MatTableModule} from '@angular/material/table';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  customerList: Employee[];
  isValid: boolean = true;
  lblNewName:any;

  constructor(private service: OrderService,
    private dialog:MatDialog,
    private customerService: EmployeeServiceService,
    private toastr : ToastrService,
    private router : Router,
    private currentRoute : ActivatedRoute
    ) { }

  ngOnInit() {
    let orderID = this.currentRoute.snapshot.paramMap.get('id');
    if(orderID==null)
    this.resetForm();
    else{
      this.service.getOrderByID(parseInt(orderID)).then( res=> {
        //console.log(res);
        this.service.formData = res.order;
        this.service.orderItems = res.item;
        this.service.formData.DeletedOrderItemIDs ="";
        
        this.userBalance = res.order['Balance'];
        this.labelString = this.getLabel(res.order['Status']);
        this.labelBalance = this.labelString;
      });
    }
    

    this.customerService.getCustomerList().then(res => {this.customerList = res as Employee[]})
  }

  public userBalance:any;
  public labelBalance:any;
  public str:any;
  public labelString:any;

  resetForm(form?: NgForm){
    if(form=null)
    form.resetForm();
    this.service.formData={
      OrderID:null,
      OrderNo:Math.floor(100000+Math.random()*900000).toString(),
      EmployeeID:0,
      PMethod:'',
      GTotal:0,
      Status:0,
      DeletedOrderItemIDs:''
    };
    this.service.orderItems = [];
  }

  AddOrEditOrderItem(orderItemIndex, OrderID){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;
    dialogConfig.width="50%";
    dialogConfig.data = {orderItemIndex, OrderID}
    this.dialog.open(OrderItemsComponent, dialogConfig).afterClosed().subscribe(res => {
      this.updateGrandTotal();
    });
  }

  onDeleteOrderItem(orderItemID: number, i: number){
    if(orderItemID!=null)
    this.service.formData.DeletedOrderItemIDs +=orderItemID +","
    
    this.service.orderItems.splice(i,1);
    this.updateGrandTotal();
  }

  updateGrandTotal(){
    this.service.formData.GTotal = this.service.orderItems.reduce((prev,curr)=>{
      return prev+curr.Total;
    },0);
    this.service.formData.GTotal = parseFloat(this.service.formData.GTotal.toFixed(2));
  }

  validateForm(){
    this.isValid = true;
    if(this.service.formData.EmployeeID==0)
    this.isValid = false;
    else if(this.service.orderItems.length==0)
    this.isValid = false;
    return this.isValid;
  }

  onSubmit(form: NgForm){
    if(this.validateForm()){
       //console.log(this.service.formData);
      // console.log(this.service.orderItems);
      
      this.service.saveorUpdateOrder().subscribe(res =>{
        console.log(res);
        this.resetForm();
        this.toastr.success("Inserted Successfully", "Restaurant App");
        this.router.navigate(['/orders']);

      })
    }
  }

  getBalance(ctrl, lblName, customerBalance){
    if(ctrl.selectedIndex==0){
      this.str="0.00";
      this.labelString = "No Balance";
    }else{
      this.labelString = this.getLabel(this.customerList[ctrl.selectedIndex-1].Status);
      
      this.str = this.customerList[ctrl.selectedIndex-1].Balance;
      
    }
    this.userBalance = this.str;
    this.labelBalance = this.labelString;
  }

  getLabel(status){
    switch (status) {
        case "1":
          return this.labelString = "Advance";
          break;
        case "2":
          return this.labelString = "Previous Balance";
      
        default:
          return this.labelString = "No Balance";
          break;
        }
  }

}
