import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-history-order-details',
  templateUrl: './history-order-details.component.html',
  styleUrls: ['./history-order-details.component.css']
})
export class HistoryOrderDetailsComponent implements OnInit {
  public emp:any;

  constructor(
    private service : EmployeeServiceService,
    private router : Router,
    private currentRoute : ActivatedRoute
  ) { }

  ngOnInit() {
    let empID = this.currentRoute.snapshot.paramMap.get('id');
    this.emp = this.service.getEmpployeeDetails(parseInt(empID));
    this.service.formData = Object.assign({},this.emp);
console.log(this.emp);
    
     
  }
  

  
}
