import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryEmployeeOrderComponent } from './history-employee-order.component';

describe('HistoryEmployeeOrderComponent', () => {
  let component: HistoryEmployeeOrderComponent;
  let fixture: ComponentFixture<HistoryEmployeeOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryEmployeeOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryEmployeeOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
