import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/shared/order.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { OrderItemsComponent } from 'src/app/orders/order-items/order-items.component';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { Employee } from 'src/app/shared/employee.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-history-employee-order',
  templateUrl: './history-employee-order.component.html',
  styleUrls: ['./history-employee-order.component.css']
})
export class HistoryEmployeeOrderComponent implements OnInit {
  customerList: Employee[];
  isValid: boolean = true;

  constructor(private service: OrderService,
    private dialog:MatDialog,
    private customerService: EmployeeServiceService,
    private router : Router,
    private currentRoute : ActivatedRoute) { }

  ngOnInit() {
    this.customerService.refreshList();
  }

  openhistory(EmployeeID : number){
    this.router.navigate(['/history/employee/'+EmployeeID]);

  }

}
