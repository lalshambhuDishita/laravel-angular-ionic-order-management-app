import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor(private router: Router) { }
  title = 'EmployeeCRUD';

  ngOnInit() {
  }

  setActive(menu){
  	$('li').removeClass();
  	$('#'+menu).addClass("active");
  }

  logoutme(){
    localStorage.removeItem('userToken');
    this.router.navigate(["/login"]);
  }

}
