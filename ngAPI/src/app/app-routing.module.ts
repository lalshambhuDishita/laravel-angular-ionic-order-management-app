import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { EmployeesComponent } from './employees/employees/employees.component';
import { EmployeeListComponent } from './employees/employees/employee-list/employee-list.component';
import { EmployeeComponent } from './employees/employees/employee/employee.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderComponent } from './orders/order/order.component';
import { PrintLayoutComponent } from './orders/print-layout/print-layout.component';
import { ItemsComponent } from './items/items.component';
import { ItemListComponent } from './items/item-list/item-list.component';
import { ItemComponent } from './items/item/item.component';
import { HistoryEmployeeOrderComponent } from './history/history-employee-order/history-employee-order.component';
import { HistoryOrderDetailsComponent } from './history/history-order-details/history-order-details.component';
import { SignInComponent } from "./user/sign-in/sign-in.component";
import { SignUpComponent } from "./user/sign-up/sign-up.component";
import {UserComponent } from "./user/user.component";
import {NavbarComponent } from "./navbar/navbar.component";
import { AuthGuard } from "./auth/auth.guard";



const routes: Routes = [
  {path: "", redirectTo: "login", pathMatch:'full'},
  
  {
    path : "signup" , component : UserComponent,
    children: [{ path:"" , component : SignUpComponent }]
  },
  {
    path: "login", component: UserComponent,
    children: [{path:"", component: SignInComponent}]
  },
  {path: "user", component :  UserComponent},
  {path: "navbar", component : NavbarComponent, canActivate:[AuthGuard]},
  {path: "employees", component : EmployeesComponent, canActivate:[AuthGuard]},
  {path: "items", component : ItemsComponent, canActivate:[AuthGuard]},
  {path: "orders", component: OrdersComponent, canActivate:[AuthGuard]},
  {path: "order", children:[
    {path:"", component:OrderComponent},
    {path:"edit/:id", component:OrderComponent}
  ], canActivate:[AuthGuard]},
  {path: "history", children:[
    {path:"", component:HistoryEmployeeOrderComponent},
    {path:"employee/:id", component:HistoryOrderDetailsComponent}
  ], canActivate:[AuthGuard]},
  { path: 'print',
    outlet: 'print',
    component: PrintLayoutComponent,
    children: [
      { path: 'order/edit/:id', component: OrderComponent , canActivate:[AuthGuard]}
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
export const routingComponents = [EmployeesComponent, EmployeeListComponent, EmployeeComponent, OrdersComponent, ItemsComponent, ItemListComponent, ItemComponent, HistoryEmployeeOrderComponent, UserComponent, SignInComponent, SignUpComponent, NavbarComponent];
