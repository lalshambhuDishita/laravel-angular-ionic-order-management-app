import { Injectable } from '@angular/core';
import { Item } from "src/app/shared/item.model";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  formData : Item;
  selectedFile:File;
  list: Item[];

  constructor(private http: HttpClient) { }

  getItemList(){
    return this.http.get(environment.apiURL+"/itemsList").toPromise();
  }

  refreshList(){
    this.http.get(environment.apiURL+"/itemsList")
    .toPromise().then(res => this.list = res as Item[])
  }

  postItem(formData : Item){
    var myFormData = new FormData();
    var price = formData.Price.toString();
    
    const headers = new HttpHeaders();
          headers.append('Content-Type', 'multipart/form-data');
          headers.append('Accept', 'application/json');
          
          myFormData.append('Image', formData.Image);
          myFormData.append('Name', formData.Name);
          myFormData.append("Price", price);
    return this.http.post(environment.apiURL+"/storeItem",myFormData,{headers:headers});

  }

  putItem(formData : Item){
    return this.http.put(environment.apiURL+"/updateItem",formData);
  }

  deleteItem(id : number){
    return this.http.delete(environment.apiURL+"/deleteItem/"+id);
  }
}
