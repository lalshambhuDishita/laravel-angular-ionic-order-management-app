export class OrderItem {
    OrderItemID:number;
    ItemID:number;
    OrderID: number;
    Quantity: number;
    ItemName: string;
    Price:number;
    Total: number;
}
