import { Injectable } from '@angular/core';
import { Order } from './order.model';
import { OrderItem } from './order-item.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  formData:Order;
  orderItems: OrderItem[];

  constructor(private http:HttpClient) { }

  saveorUpdateOrder(){
    var body = {
      ...this.formData,
      OrderItems : this.orderItems
    };
    return this.http.post(environment.apiURL+"/orders",body);
  }

  getOrderList(){
    return this.http.get(environment.apiURL+"/orders").toPromise();
  }

  getOrderByID(id:number):any{
    return this.http.get(environment.apiURL+"/orders/"+id).toPromise();
  }

  deleteOrder(orderID:number){
    return this.http.delete(environment.apiURL+"/destroyorder/"+orderID).toPromise();
  }
}
