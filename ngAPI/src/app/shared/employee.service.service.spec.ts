import { TestBed } from '@angular/core/testing';

import { Employee.ServiceService } from './employee.service.service';

describe('Employee.ServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Employee.ServiceService = TestBed.get(Employee.ServiceService);
    expect(service).toBeTruthy();
  });
});
