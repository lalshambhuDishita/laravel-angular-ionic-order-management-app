export class Order {
    OrderID:number;
    OrderNo: string;
    EmployeeID: number;
    PMethod: string;
    GTotal: number;
    Status: number;
    DeletedOrderItemIDs: string;
}
