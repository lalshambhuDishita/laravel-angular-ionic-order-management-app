import { Injectable } from '@angular/core';
import { User } from 'src/app/shared/user.model';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  formData: User;
  constructor(
    private http: HttpClient
    ) { }

  userAuthentication(userName, password){
    var data = {"username":userName, "password":password};
    const headers = new HttpHeaders();
          headers.append('Content-Type', 'multipart/form-data');
          headers.append('Accept', 'application/json');
    return this.http.post(environment.apiURL+"/login",data,{headers: headers});
  }
}
