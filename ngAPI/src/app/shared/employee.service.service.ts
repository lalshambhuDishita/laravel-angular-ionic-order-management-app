import { Injectable } from '@angular/core';
import { Employee } from 'src/app/shared/employee.model';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {
  formData: Employee;
  list: Employee[];
  readonly rootURL = "http://empployee.local/api";

  constructor(private http : HttpClient) { }

  postEmployee(formData : Employee){
    return this.http.post(this.rootURL+"/storeEmployees",formData);

  }
  refreshList(){
    this.http.get(this.rootURL+"/employeesList")
    .toPromise().then(res => this.list = res as Employee[])
  }

  putEmployee(formData : Employee){
    return this.http.put(this.rootURL+"/updateEmployee",formData);
  }

  deleteEmpployee(id : number){
    return this.http.delete(this.rootURL+"/deleteEmployee/"+id);
  }

  getCustomerList(){
    return this.http.get(this.rootURL+"/employeesList").toPromise();
  }

  getEmpployeeDetails(id : number){
    return this.http.get(this.rootURL+"/showEmployee/"+id).toPromise().then(res => this.formData = res as Employee)
  }
}
