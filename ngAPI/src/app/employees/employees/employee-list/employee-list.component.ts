import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { Employee } from 'src/app/shared/employee.model';
import { ToastrService } from 'ngx-toastr';
import {MatTableModule} from '@angular/material/table';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  constructor(private service : EmployeeServiceService,
    private toastr : ToastrService) { }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(emp : Employee){
    this.service.formData = Object.assign({},emp);
  }

  onDelete(id : number){
    if(confirm("Are you sure to delete this record?")){
        this.service.deleteEmpployee(id).subscribe(res =>{
        this.service.refreshList();
        this.toastr.warning("Employee Deleted succesfully", "Emp Deleted");
      })
    }
    
  }

  orderhistory(empID : number){

  }

}