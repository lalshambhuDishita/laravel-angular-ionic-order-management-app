import { Component, OnInit } from '@angular/core';
import { EmployeeServiceService } from 'src/app/shared/employee.service.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import {MatRadioModule} from '@angular/material/radio';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(
    private service : EmployeeServiceService,
    private toastr : ToastrService
    ) { }

  ngOnInit() {
    this.resetForm();
  }
  public temp:any;

  resetForm(form? : NgForm){
    if(form!=null)
    form.resetForm();
    this.service.formData = {
      EmployeeID : null,
      FullName :'',
      EMPCode :'',
      Mobile :'',
      Balance :0,
      Status: 0,
      email: '',
      password: ''

    }
  }

  onSubmit(form : NgForm){
    if(form.value.EmployeeID==null)
    this.insertRecord(form);
    else
    this.updateRecord(form);
  }

  insertRecord(form : NgForm){
    this.service.postEmployee(form.value).subscribe(res =>{
      this.toastr.success("Inserted Successfully", "Emp Register");
      this.resetForm(form);
      this.service.refreshList();
    })
  }

  updateRecord(form : NgForm){
    this.service.putEmployee(form.value).subscribe(res =>{
      //console.log(res);
      this.toastr.info("Updated Successfully", "Emp Updated");
      this.resetForm(form);
      this.service.refreshList();
    })
  }

addmoreadvance(){
  
  var st= "ok";
  Object.assign({},st);console.log(st);
  
}

  RadioChange(ctrl, Bal){
      if(ctrl==0 || ctrl=="0"){
        this.service.formData.Balance = 0;
      }
    }
}
