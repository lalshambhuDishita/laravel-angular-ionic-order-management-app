import { Component, OnInit } from '@angular/core';
import { UserService } from "src/app/shared/user.service";
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HttpErrorResponse } from "@angular/common/http";


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  isLoginError:boolean = false;

  constructor(
    private UserService:UserService,
    private toastr : ToastrService,
    private router : Router
    ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm){
    if(form!=null)
    form.resetForm();
    this.UserService.formData = {
      id : null,
      name :'',
      email :'',
      password :''

    }
  }

  onSubmit(UserName, Password){
    this.UserService.userAuthentication(UserName,Password).subscribe((data:any)=>{
      localStorage.setItem('userToken',data.access_token);
      this.router.navigate(["/employees"]);
    },
    (err:HttpErrorResponse)=>{
      this.isLoginError = true;
    }
    );
  }

}
