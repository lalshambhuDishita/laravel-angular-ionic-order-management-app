import { Component, OnInit } from '@angular/core';
import { UserService } from "src/app/shared/user.service";
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  

  constructor(
    private UserService:UserService,
    private toastr : ToastrService
  ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm){
    if(form!=null)
    form.resetForm();
    this.UserService.formData = {
      id : null,
      name :'',
      email :'',
      password :''

    }
  }

}
