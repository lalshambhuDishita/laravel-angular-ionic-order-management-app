import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";

import { EmployeesComponent } from '../employees/employees/employees.component';
import { OrdersComponent } from '../orders/orders.component';



import { EmployeeComponent } from '../employees/employees/employee/employee.component';
import { EmployeeListComponent } from '../employees/employees/employee-list/employee-list.component';

const routes: Routes = [
  {path: "employees", component : EmployeesComponent},
  {path: "orders", component: OrdersComponent}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
export const routingComponents = [EmployeesComponent, EmployeeComponent, EmployeeListComponent, OrdersComponent];
