import { Component, OnInit } from '@angular/core';
import { ItemService } from 'src/app/shared/item.service';
import { Item } from 'src/app/shared/item.model';
import { ToastrService } from 'ngx-toastr';
import {MatTableModule} from '@angular/material/table';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styles: []
})
export class ItemListComponent implements OnInit {

  constructor(
    private service : ItemService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.service.refreshList();
  }

  populateForm(item : Item){
    this.service.formData = Object.assign({},item);
  }

  onDelete(id : number){
    if(confirm("Are you sure to delete this record?")){
        this.service.deleteItem(id).subscribe(res =>{
        this.service.refreshList();
        this.toastr.warning("Item Deleted succesfully", "Item Deleted");
      })
    }
    
  }

}
