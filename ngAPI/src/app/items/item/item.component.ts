import { Component, OnInit } from '@angular/core';
import { ItemService } from "src/app/shared/item.service";
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styles: []
})
export class ItemComponent implements OnInit {

  selectedFile:any;

  constructor(
    private service : ItemService,
    private toastr : ToastrService
  ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm){
    if(form!=null)
    form.resetForm();
    this.service.formData = {
      ItemID : null,
      Name :'',
      Price : 0,
      Image :''

    }
  }

  onFileSelected(event){
    this.selectedFile = new FormData();
    this.selectedFile = <File>event.target.files[0];
     //console.log(this.selectedFile);
  }

  onSubmit(form : NgForm){
    if(form.value.ItemID==null){

      const formData = {
        Image: this.selectedFile,
        Name: form.value.Name,
        Price: form.value.Price
      };
      
      form.value.Image = formData.Image;
      form.value.Name = formData.Name;
      form.value.Price = parseFloat(formData.Price).toFixed(2);

      //console.log(form.value);
      this.insertRecord(form);
    }
    else{
      this.updateRecord(form);
    }
      
  }

  insertRecord(form : NgForm){
    //form.value.Image=this.selectedFile;
    this.service.postItem(form.value).subscribe(res =>{
      console.log(res);
      this.toastr.success("Inserted Successfully", "Item Register");
      this.resetForm(form);
      this.service.refreshList();
    })
  }

  updateRecord(form : NgForm){
    this.service.putItem(form.value).subscribe(res =>{
      this.toastr.info("Updated Successfully", "Item Updated");
      this.resetForm(form);
      this.service.refreshList();
    })
  }

}
